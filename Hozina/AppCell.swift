//
//  AppCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class AppCell: UITableViewCell {
  
  @IBOutlet weak var iconImageView: BorderCornerImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var detailLabel: UILabel!
  
  func display(appObject: AppObject) {
    if let icon = appObject.photo {
      if let url: URL = URL(string: icon as String) {
        iconImageView.kf.setImage(with: url)
      } else {
        iconImageView.kf.setImage(with: nil)
      }
    } else {
      iconImageView.kf.setImage(with: nil)
    }
    if let name = appObject.name {
      nameLabel.text = name as String
    } else {
      nameLabel.text = ""
    }
    if let detail = appObject.detail {
      detailLabel.text = detail as String
    } else {
      detailLabel.text = ""
    }
  }
}
