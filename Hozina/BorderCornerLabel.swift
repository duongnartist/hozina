//
//  BorderCornerLabel.swift
//  Grocery
//
//  Created by Nguyễn Dương on 8/6/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

@IBDesignable
class BorderCornerLabel: UILabel {
  
  @IBInspectable
  var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
    }
  }
  
  @IBInspectable
  var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable
  var borderColor: UIColor? {
    get {
      if let color = layer.borderColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set {
      if let color = newValue {
        layer.borderColor = color.cgColor
      } else {
        layer.borderColor = nil
      }
    }
  }
  
  //  @IBInspectable var firstColor:UIColor = UIColor.clear
  //  @IBInspectable var secondColor:UIColor = UIColor.clear
  //  @IBInspectable var startPoint:CGPoint = CGPoint(x: 0.0, y: 1.0)
  //  @IBInspectable var endPoint:CGPoint = CGPoint(x: 1.0, y:0.0)
  //
  //  var gradientLayer:CAGradientLayer!
  //
  //  override func draw(_ rect: CGRect) {
  //    super.draw(rect)
  //
  //    gradientLayer = CAGradientLayer()
  //    self.gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
  //    self.gradientLayer.startPoint = self.startPoint
  //    self.gradientLayer.endPoint = self.endPoint
  //    self.gradientLayer.frame = self.frame
  //    self.layer.insertSublayer(self.gradientLayer, at: 0)
  //  }
  
}
