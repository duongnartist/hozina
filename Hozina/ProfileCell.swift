//
//  ProfileCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/11/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZAlertController

class ProfileCell: UICollectionViewCell {
  
  @IBOutlet weak var coverImageView: UIImageView!
  @IBOutlet weak var avatarImageView: BorderCornerImageView!
  @IBOutlet weak var fullnameLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var count0Label: UILabel!
  @IBOutlet weak var count1Label: UILabel!
  @IBOutlet weak var count2Label: UILabel!
  
  @IBOutlet weak var followHeight: NSLayoutConstraint!
  @IBOutlet weak var followButton: BorderCornerButton!
  var userObject: UserObject!
  var isRequesting = false
  
  func display(userObject: UserObject, isMe: Bool) {
    self.userObject = userObject
    
    followButton.isHidden = isMe
    followHeight.constant = isMe ? 0 : 30
    if !isMe {
      followButton.setTitle(userObject.isFavorite() ? "status_unfollow".localized() : "status_follow".localized(), for: .normal)
    }
    
    if let name = userObject.name {
      fullnameLabel.text = name as String
    } else {
      fullnameLabel.text = ""
    }
    if let address = userObject.address {
      addressLabel.text = address as String
    } else {
      addressLabel.text = ""
    }
    
    let photo = userObject.getAvatar()
    if let url = URL(string: photo) {
      coverImageView.kf.setImage(with: url)
      avatarImageView.kf.setImage(with: url)
    } else {
      coverImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
      avatarImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
    }
    
    if let value = userObject.number_enjoying {
      count0Label.text = "\(value)"
    } else {
      count0Label.text = "0"
    }
    if let value = userObject.number_favorite {
      count1Label.text = "\(value)"
    } else {
      count1Label.text = "0"
    }
    
    if let value = userObject.number_share {
      count2Label.text = "\(value)"
    } else {
      count2Label.text = "0"
    }
  }
  
  @IBAction func followButtonTouchUpInside(_ sender: Any) {
    guard isRequesting else {
      if userObject != nil {
        if userObject.isFavorite() {
          EZAlertController.alert("title_confirm".localized(), message: String(format: "message_unfollow".localized(), userObject._name), buttons: ["btn_no".localized(), "btn_yes".localized()], tapBlock: { alert, index in
            if index > 0 {
              self.follow()
            }
          })
        } else {
          follow()
        }
      }
      return
    }
  }
  
  func follow() {
    if userObject != nil {
      isRequesting = true
      UIApplication.shared.isNetworkActivityIndicatorVisible = true
      Network.addUserFavorite(userObject: userObject, response: { res in
        self.isRequesting = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        if res.isSuccess {
          self.userObject.is_favorite = (self.userObject.isFavorite() ? 0 : 1) as NSNumber
          self.followButton.setTitle(self.userObject.isFavorite() ? "status_unfollow".localized() : "status_follow".localized(), for: .normal)
        }
      })
    }
  }
}
