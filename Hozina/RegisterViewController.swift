//
//  RegisterViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/8/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZAlertController
import AccountKit

class RegisterViewController: UIViewController {
  
  @IBOutlet weak var phoneTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var reenterTextField: UITextField!
  
  fileprivate var accountKit = AKFAccountKit(responseType: .accessToken)
  fileprivate var pendingLoginViewController: AKFViewController?
  fileprivate var showAccountOnAppear = false
  
  var requesting = false
  var userObject = UserObject()
  
  override func loadView() {
    super.loadView()
    //    print("RegisterViewController -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print("RegisterViewController -> viewDidLoad")
    showAccountOnAppear = accountKit.currentAccessToken != nil
    pendingLoginViewController = accountKit.viewControllerForLoginResume() as? AKFViewController
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print("RegisterViewController -> viewWillAppear")
    if showAccountOnAppear {
      showAccountOnAppear = false
    } else if let viewController = pendingLoginViewController {
      prepareLoginViewController(viewController)
      if let viewController = viewController as? UIViewController {
        present(viewController, animated: animated, completion: nil)
        pendingLoginViewController = nil
      }
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print("RegisterViewController -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print("RegisterViewController -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print("RegisterViewController -> viewDidDisappear")
  }
  
  deinit {
    //    print("RegisterViewController -> deinit")
  }
  
  func register(userObject: UserObject) {
    guard requesting else {
      show(requesting: &requesting)
      Network.register(userObject: userObject, response: {
        res in
        self.hide(requesting: &self.requesting)
        if res.code == 200 {
          self.onLoginSuccess()
        } else {
          self.onLoginFailure(message: res.message! as String)
        }
      })
      return
    }
  }
  
  func onLoginSuccess() {
    view.window?.rootViewController = "MainViewController".viewController(storyboard: "Main")
  }
  
  func onLoginFailure(message: String) {
    EZAlertController.alert("alert_title".localized(), message: message, acceptMessage: "btn_ok".localized(), acceptBlock: {
      
    })
  }
  
  fileprivate func prepareLoginViewController(_ loginViewController: AKFViewController) {
    let theme: AKFTheme = AKFTheme.default()
    theme.headerBackgroundColor = UIColor(hexString: "#0abf5d")!
    theme.headerTextColor = UIColor(hexString: "#ffffff")!
    theme.iconColor = UIColor(hexString: "#0abf5d")!
    theme.inputTextColor = UIColor(hexString: "#0abf5d")!
    theme.statusBarStyle = .lightContent
    theme.textColor = UIColor(hexString: "#000000")!
    theme.titleColor = UIColor(hexString: "#000000")!
    theme.backgroundColor = UIColor(hexString: "#f2f1f1")!
    theme.inputBorderColor = UIColor(hexString: "#0abf5d")!
    theme.inputBackgroundColor = UIColor(hexString: "#ffffff")!
    theme.buttonTextColor = UIColor(hexString: "#ffffff")!
    theme.buttonBackgroundColor = UIColor(hexString: "#0abf5d")!
    theme.buttonBorderColor = UIColor(hexString: "#0abf5d")!
    theme.buttonDisabledBackgroundColor = UIColor(hexString: "#ffffff")!
    theme.buttonDisabledTextColor = UIColor(hexString: "#0abf5d")!
    theme.buttonDisabledBorderColor = UIColor(hexString: "#0abf5d")!
    loginViewController.setTheme(theme)
    loginViewController.delegate = self
  }
  
  @IBAction func registerButtonTouchUpInside(_ sender: Any) {
    guard requesting else {
      preapreUserObject(success: { _ in
        if let viewController = accountKit.viewControllerForPhoneLogin(with: nil, state: nil) as? AKFViewController {
          prepareLoginViewController(viewController)
          if let viewController = viewController as? UIViewController {
            present(viewController, animated: true, completion: nil)
          }
        }
      }, failure: { key, value in
        EZAlertController.alert("alert_title".localized(), message: value, acceptMessage: "btn_ok".localized(), acceptBlock: {
          switch key {
          case 0:
            self.phoneTextField.becomeFirstResponder()
            break
          case 1:
            self.passwordTextField.becomeFirstResponder()
            break
          case 2:
            self.reenterTextField.becomeFirstResponder()
            break
          default:
            break
          }
        })
      })
      return
    }
  }
  
  func preapreUserObject(success: (_ userObject: UserObject) -> (), failure: (_ key: Int, _ value: String) -> ()) {
    userObject.phone_number = phoneTextField.text! as NSString
    userObject.password = passwordTextField.text! as NSString
    if (userObject.phone_number?.length)! >= 10 {
      if (userObject.password?.length)! >= 8 {
        let reenter = reenterTextField.text! as NSString
        if reenter == userObject.password {
          success(userObject)
        } else {
          failure(2, "invalid_pass".localized())
        }
      } else {
        failure(1, "invalid_pass".localized())
      }
    } else {
      failure(0, "invalid_phone".localized())
    }
  }
  
  @IBAction func loginButtonTouchUpInside(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
}

extension RegisterViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}

extension RegisterViewController: AKFViewControllerDelegate {
  
  func viewController(_ viewController: UIViewController!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
    register(userObject: userObject)
  }
  
  func viewController(_ viewController: UIViewController!, didFailWithError error: Error!) {
    print("\(viewController) did fail with error: \(error)")
  }
}
