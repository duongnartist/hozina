//
//  FollowerCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/11/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class FollowerCell: UITableViewCell {
  
  @IBOutlet weak var avatarImageView: BorderCornerImageView!
  
  @IBOutlet weak var statusImageView: BorderCornerImageView!
  @IBOutlet weak var nameLabel: UILabel!
  
  func display(followerObject: FollowerObject) {
    if let value = followerObject.name {
      nameLabel.text = value as String
    }
    if let value = followerObject.photo {
      avatarImageView.kf.setImage(with: URL(string: value as String)!)
    }
    // TODO
//    statusImageView.image = followerObject.isFriend ? Icon_Correct : Icon_Add_User
  }
  
}
