//
//  ManagerTableViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZAlertController
import EZSwiftExtensions

class ManagerTableViewController: UITableViewController {
  
  @IBOutlet weak var coverImageView: UIImageView!
  
  @IBOutlet weak var avatarImageView: BorderCornerImageView!
  
  @IBOutlet weak var fullnameLabel: UILabel!
  
  @IBOutlet weak var fullnameTextField: UITextField!
  @IBOutlet weak var birthdayTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var cityTextField: UITextField!
  @IBOutlet weak var descriptionTextField: UITextView!
  @IBOutlet weak var cameraButton: UIButton!
  
  @IBOutlet weak var versionLabel: UILabel!
  
  var imagePickerController: UIImagePickerController? = UIImagePickerController()
  var requesting = false
  var userProfile = UserObject()
  var birthdayDate = Date()
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
    fullnameTextField.delegate = self
    birthdayTextField.delegate = self
    emailTextField.delegate = self
    cityTextField.delegate = self
    imagePickerController?.delegate = self
    descriptionTextField.make(borderWidth: 0.5, borderColor: UIColor.lightGray.cgColor)
    descriptionTextField.make(cornerRadius: 6)
    userProfile = UserObject.userObject.clone()
    displayUserProfile()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  func displayUserProfile() {
    if let birthday = userProfile.birthday {
      let birthdayString = birthday as String
      if !birthdayString.isEmpty {
        if birthdayString.length > 10 {
          birthdayDate = Date(fromString: birthdayString as String, format: "yyyy-MM-dd hh:mm:ss")!
        } else {
          birthdayDate = Date(fromString: birthdayString as String, format: "yyyy-MM-dd")!
        }
        birthdayTextField.text = birthdayDate.toString(format: "dd-MM-yyyy")
      } else {
        birthdayTextField.text = birthdayDate.toString(format: "dd-MM-yyyy")
      }
    } else {
      birthdayTextField.text = birthdayDate.toString(format: "dd-MM-yyyy")
    }
    
    let photo = userProfile.getAvatar()
    if let url = URL(string: photo as String) {
      avatarImageView.kf.setImage(with: url)
      coverImageView.kf.setImage(with: url)
    } else {
      avatarImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
      coverImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
    }
    
    if let name = userProfile.name {
      fullnameTextField.text = name as String
      fullnameLabel.text = name as String
    } else {
      fullnameTextField.text = ""
      fullnameLabel.text = ""
    }
    
    if let email = userProfile.email {
      emailTextField.text = email as String
    } else {
      emailTextField.text = ""
    }
    
    if let address = userProfile.address {
      cityTextField.text = address as String
    } else {
      cityTextField.text = ""
    }
    
    if let description_vi = userProfile.description_vi {
      descriptionTextField.text = description_vi as String
    } else {
      descriptionTextField.text = ""
    }
  }
  
  func prepareUserProfile(complete: (_ userProfile: UserObject, _ index: Int, _ value: String) -> Void) {
    let userObject = UserObject()
    userObject.name = fullnameTextField.text! as NSString
    if (userObject.name?.length)! < 1 {
      complete(userObject, 0, "invalid_username".localized())
      return
    }
    
    userObject.birthday = birthdayDate.toString(format: "yyyy-MM-dd") as NSString
    if (userObject.birthday?.length)! < 1 {
      complete(userObject, 1, "invalid_birthday".localized())
      return
    }
    
    userObject.email = emailTextField.text as NSString?
    if (userObject.email?.length)! < 1 {
      complete(userObject, 3, "invalid_email".localized())
      return
    }
    
    userObject.address = cityTextField.text! as NSString
    if (userObject.address?.length)! < 1 {
      complete(userObject, 4, "invalid_address".localized())
      return
    }
    
    userObject.description_vi = descriptionTextField.text! as NSString
    if (userObject.description_vi?.length)! < 1 {
      complete(userObject, 5, "invalid_description".localized())
      return
    }
    complete(userObject, -1, "")
  }
  
  func done() {
    guard requesting else {
      prepareUserProfile(complete: { userObject, index, value in
        if index < 0 {
          show(requesting: &self.requesting)
          userObject.photo = UserObject.userObject.photo
          Network.updateProfile(userObject: userObject, response: { res in
            self.hide(requesting: &self.requesting)
            if res.isSuccess {
              self.userProfile = res.result!
              self.navigationController?.popViewController(animated: true)
            } else {
              EZAlertController.alert("alert_title".localized(), message: res._message, acceptMessage: "btn_ok".localized(), acceptBlock: {
                
              })
            }
          })
        } else {
          EZAlertController.alert("alert_title".localized(), message: value, acceptMessage: "btn_ok".localized(), acceptBlock: {
            switch index {
            case 0:
              self.fullnameTextField.becomeFirstResponder()
              break
            case 1:
              self.birthdayTextField.becomeFirstResponder()
              break
            case 2:
              self.emailTextField.becomeFirstResponder()
              break
            case 3:
              self.cityTextField.becomeFirstResponder()
              break
            case 4:
              self.descriptionTextField.becomeFirstResponder()
              break
            default:
              break
            }
          })
        }
      })
      return
    }
  }
  
  @IBAction func cameraButtonAction(_ sender: Any) {
    guard requesting else {
      showImagePicker()
      return
    }
  }
  
  @IBAction func feedbackButtonTouchUpInside(_ sender: Any) {
  }
}

extension ManagerTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func showImagePicker() {
    let optionMenu = UIAlertController(
      title: "title_change_avatar".localized(),
      message: "title_choose_image".localized(),
      preferredStyle: .actionSheet
    )
    let cameraAction = UIAlertAction(
      title: "btn_camera".localized(),
      style: .default,
      handler: {
        (_: UIAlertAction!) -> Void in
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
          self.openCamera(self.view)
        } else {
          self.openLibrary(self.view)
        }
      }
    )
    let libraryAction = UIAlertAction(
      title: "btn_library".localized(),
      style: .default,
      handler: {
        (_: UIAlertAction!) -> Void in
        self.openLibrary(self.view)
      }
    )
    let albumAction = UIAlertAction(
      title: "btn_album".localized(),
      style: .default,
      handler: {
        (_: UIAlertAction!) -> Void in
        self.openAlbum(self.view)
      }
    )
    let cancelAction = UIAlertAction(
      title: "btn_cancel".localized(),
      style: .cancel,
      handler: {
        (_: UIAlertAction!) -> Void in
        return
      }
    )
    optionMenu.addAction(cameraAction)
    optionMenu.addAction(libraryAction)
    optionMenu.addAction(albumAction)
    optionMenu.addAction(cancelAction)
    present(optionMenu, animated: true, completion: nil)
  }
  
  func openCamera(_ sender: AnyObject) {
    imagePickerController!.sourceType = UIImagePickerControllerSourceType.camera
    present(imagePickerController!, animated: true, completion: nil)
  }
  
  func openLibrary(_ sender: AnyObject) {
    imagePickerController!.sourceType = UIImagePickerControllerSourceType.photoLibrary
    present(imagePickerController!, animated: true, completion: nil)
  }
  
  func openAlbum(_ sender: AnyObject) {
    imagePickerController!.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
    present(imagePickerController!, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
    picker.dismiss(animated: true, completion: nil)
    var image = info[UIImagePickerControllerOriginalImage] as? UIImage
    image = image?.resizeWithWidth(ez.screenWidth * UIScreen.main.scale)
    avatarImageView.image = image
    coverImageView.image = image
    upload(image: image!)
  }
  
  func upload(image: UIImage) {
    guard requesting else {
      show(requesting: &requesting)
      Network.upload(images: [image], complete: { res in
        if res.isSuccess {
          let url = res.getPhoto(from: "")
          self.userProfile.photo = url as NSString
          Network.updateProfile(userObject: self.userProfile, response: { res in
            self.hide(requesting: &self.requesting)
            if res.isSuccess {
              self.userProfile = res.result!
            } else {
              EZAlertController.alert("alert_title".localized(), message: res._message, acceptMessage: "btn_ok".localized(), acceptBlock: {
                
              })
            }
          })
        } else {
          self.hide(requesting: &self.requesting)
          EZAlertController.alert("alert_title".localized(), message: res._message, acceptMessage: "btn_ok".localized(), acceptBlock: {
            
          })
        }
      })
      return
    }
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
  
  func showDatePicker() {
    let title = birthdayTextField.placeholder
    let datePicker = ActionSheetDatePicker(
      title: title,
      datePickerMode: UIDatePickerMode.date,
      selectedDate: birthdayDate,
      doneBlock: {
        _, value, _ in
        if let date = value as? Date {
          self.birthdayDate = date
          self.userProfile.birthday = self.birthdayDate.toString(format: "yyyy-MM-dd hh:mm:ss") as NSString
          self.birthdayTextField.text = self.birthdayDate.toString(format: "dd-MM-yyyy")
        }
        return
      },
      cancel: { _ in return },
      origin: birthdayTextField.superview!.superview
    )
    let doneButton = UIBarButtonItem()
    doneButton.title = "btn_done".localized()
    datePicker?.setDoneButton(doneButton)
    let cancelButton = UIBarButtonItem()
    cancelButton.title = "btn_cancel".localized()
    datePicker?.setCancelButton(cancelButton)
    datePicker?.minimumDate = Date(timeIntervalSince1970: 0)
    datePicker?.maximumDate = Date()
    datePicker?.show()
  }
}

extension ManagerTableViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if textField == birthdayTextField {
      showDatePicker()
      return false
    }
    return true
  }
  
}
