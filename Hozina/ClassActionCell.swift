//
//  ClassActionCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/16/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ClassActionCell: UITableViewCell {
  
  @IBOutlet weak var likeImageView: UIImageView!
  var classObject: ClassObject!
  var tableView: UITableView!
  
  func display(classObject: ClassObject) {
    self.classObject = classObject
    if let value = classObject.is_like {
      if (value as! Int) > 0 {
        likeImageView.image = #imageLiteral(resourceName: "Icon_Cell_Heart_Red")
      } else {
        likeImageView.image = #imageLiteral(resourceName: "Icon_Cell_Heart_Gray")
      }
    } else {
      likeImageView.image = #imageLiteral(resourceName: "Icon_Cell_Heart_Gray")
    }
  }
  
  @IBAction func likeButtonTouchUpInside(_ sender: Any) {
    if classObject != nil {
      Network.addFavorite(classObject: classObject, response: {
        res in
        if res.isSuccess {
          self.likeImageView.image = #imageLiteral(resourceName: "Icon_Cell_Heart_Red")
        }
      })
    }
  }
  
  @IBAction func commentButtonTouchUpInside(_ sender: Any) {
    if tableView != nil {
      tableView.scrollToRow(at: IndexPath(row: 0, section: 4), at: .bottom, animated: true)
      NotificationCenter.default.post(name: NSNotification.Name("NewComment"), object: nil)
    }
  }
  
  @IBAction func shareButtonTouchUpInside(_ sender: Any) {
  }
}
