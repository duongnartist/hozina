//
//  HomeViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/9/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class HomeViewController: UIViewController {
  
  var requesting = false
  var images = [String]()
  var colors = ["#ffcc5c", "#ff6f69", "#88d8b0", "#129793", "#7b8d8e", "#5c4c46", "#3d566e", "#9b59b6", "#f39c12", "#2ecc71"]
  var greenColor = UIColor(hexString: "#2ecc71")
  var categoryData = [CategoryObject]()
  var fullCategoryData = [CategoryObject]()
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  override func loadView() {
    super.loadView()
    //    print("HomeViewController -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print("HomeViewController -> viewDidLoad")
    let categoryObject = CategoryObject()
    categoryObject.name = "Gần đây"    
    CategoryObject.nearCategory = categoryObject
    
    images = ["https://cdn.pixabay.com/photo/2017/08/01/08/29/people-2563491__480.jpg", "https://cdn.pixabay.com/photo/2017/08/04/14/44/dahlias-2580236__480.png", "https://cdn.pixabay.com/photo/2017/07/15/13/21/lotus-flower-2506451__480.jpg", "https://cdn.pixabay.com/photo/2017/07/28/14/29/macarons-2548827__480.jpg", "https://cdn.pixabay.com/photo/2017/08/02/15/15/horse-2572051__480.jpg", "https://cdn.pixabay.com/photo/2017/07/14/07/44/strawberry-2502961__480.jpg"]
    
    fetchCategoryData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print("HomeViewController -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print("HomeViewController -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print("HomeViewController -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print("HomeViewController -> viewDidDisappear")
  }
  
  deinit {
    //    print("HomeViewController -> deinit")
  }
  
  func fetchCategoryData() {
    guard requesting else {
      show(requesting: &requesting)
      Network.getAllCategories(response: { res in
        self.hide(requesting: &self.requesting)
        self.categoryData = []
        self.fullCategoryData = [CategoryObject.nearCategory]
        if res.isSuccess {
          CategoryObject.categoryData = res.result
          self.categoryData += res.result
          self.fullCategoryData += res.result
          NotificationCenter.default.post(name: NSNotification.Name("FetchedCategoryData"), object: res)
        }
        self.collectionView.reloadData()
      })
      return
    }
  }
  
  override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
    if identifier == "ShowClassesByCategory" {
      if let indexPath: IndexPath = collectionView.indexPathsForSelectedItems?.first {
        if indexPath.row == 0 {
          return false
        }
      }
    }
    return true
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    prepareShowClassesByCategory(for: segue)
  }
  
  func prepareShowClassesByCategory(for segue: UIStoryboardSegue) {
    if segue.identifier == "ShowClassesByCategory" {
      if let viewController: ClassesByCategoryViewController = segue.destination as? ClassesByCategoryViewController {
        if let indexPath: IndexPath = collectionView.indexPathsForSelectedItems?.first {
          if indexPath.row > 0 {
            let selectedIndex = indexPath.row - 1
            viewController.isNoSubCategory = false
            for i in 0 ..< self.categoryData.count {
              self.categoryData[i].isSelected = i == selectedIndex
            }
            viewController.categoryObject = self.categoryData[selectedIndex]
            viewController.categoryData = self.categoryData
          } else {
            
          }
        }
      }
    }
  }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 2
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return section == 0 ? 1 : self.fullCategoryData.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if indexPath.section == 0 {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeImageSlideCell", for: indexPath) as! HomeImageSlideCell
      cell.display(images: images)
      return cell
    }
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
    cell.display(categoryObject: self.fullCategoryData.get(at: indexPath.row)!)
    if indexPath.row == 0 {
      cell.titleLabel.textColor = greenColor
      cell.backView.backgroundColor = UIColor.white
    } else {
      cell.titleLabel.textColor = UIColor.white
      cell.backView.backgroundColor = UIColor(hexString: colors[(indexPath.row - 1) % colors.count])
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    if indexPath.section == 0 {
      return CGSize(width: ez.screenWidth, height: 200)
    }
    let width = (ez.screenWidth - 30) / 2
    return CGSize(width: width, height: width)
  }
  
}
