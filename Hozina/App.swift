//
//  App.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class AppResponse: BaseResponse {
  var result: [AppObject] = []
}

class AppObject: EVObject {
  var id: NSNumber? = 0
  var name: NSString? = ""
  var detail: NSString? = ""
  var photo: NSString? = ""
}
