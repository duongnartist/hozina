//
//  TitleCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/11/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class TitleCell: UICollectionViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  
  func display(text: String) {
    titleLabel.text = text
  }
  
}
