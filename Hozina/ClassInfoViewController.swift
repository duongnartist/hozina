//
//  ClassInfoViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ClassInfoViewController: UIViewController {
  
  var classTableViewController: ClassInfoTableViewController!
  
  override func loadView() {
    super.loadView()
    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    print(" -> viewDidLoad")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    print(" -> viewDidDisappear")
  }
  
  deinit {
    print(" -> deinit")
  }
  
  @IBAction func postButtonTouchUpInside(_ sender: Any) {
    if classTableViewController != nil {
      classTableViewController.postClass()
    }
  }
  
  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    prepareEmbedClass(for: segue)
  }
  
  func prepareEmbedClass(for segue: UIStoryboardSegue) {
    if segue.identifier == "EmbedClass" {
      if let classTableViewController = segue.destination as? ClassInfoTableViewController {
        self.classTableViewController = classTableViewController
      }
    }
  }
}
