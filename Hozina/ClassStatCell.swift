//
//  ClassStatCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/16/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ClassStatCell: UITableViewCell {
  
  @IBOutlet weak var likeLabel: UILabel!
  @IBOutlet weak var commentLabel: UILabel!
  @IBOutlet weak var shareLabel: UILabel!
  
  func display(classObject: ClassObject) {
    if let value = classObject.number_like {
      likeLabel.text = "\(value)"
    } else {
      likeLabel.text = "0"
    }
    
    if let value = classObject.number_comment {
      commentLabel.text = String(format: "class_comment".localized(), value as! Int)
    } else {
      commentLabel.text = String(format: "class_comment".localized(), 0)
    }
    
    if let value = classObject.number_share {
      shareLabel.text = String(format: "class_share".localized(), value as! Int)
    } else {
      shareLabel.text = String(format: "class_share".localized(), 0)
    }
  }
}
