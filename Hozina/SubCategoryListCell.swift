//
//  SubCategoryListCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class SubCategoryListCell: UICollectionViewCell {
  
  @IBOutlet weak var collectionView: UICollectionView!
  var data = [SubCategoryObject]()
  var selectSubCategoryAction: ((_ index: Int) -> ())!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView.delegate = self
    collectionView.dataSource = self
  }
  
  func display(data: [SubCategoryObject]) {
    self.data = data
    self.collectionView.reloadData()
  }
  
}

extension SubCategoryListCell: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return data.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
    cell.display(subCategoryObject: data.get(at: indexPath.row)!)
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if selectSubCategoryAction != nil {
      selectSubCategoryAction(indexPath.row)
    }
  }
}
