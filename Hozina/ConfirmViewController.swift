//
//  ConfirmViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/16/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ConfirmViewController: UIViewController {
  
  var selectAction: ((_ index: Int) -> ())!
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  @IBAction func yesButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: {
      if self.selectAction != nil {
        self.selectAction(1)
      }
    })
  }
  
  @IBAction func noButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: {
      if self.selectAction != nil {
        self.selectAction(0)
      }
    })
  }
}
