//
//  SettingsTableViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
  
  var greenColor = UIColor(hexString: "0ABF5D")
  
  @IBOutlet weak var giftCodeLabel: UILabel!
  @IBOutlet weak var vnButton: BorderCornerButton!
  @IBOutlet weak var enButton: BorderCornerButton!
  
  var shareAction: (() -> ())!
  var selectedLanguage = 0
  var selectLanguageAction: ((_ index: Int) -> ())!
  var selectMenuAction: ((_ index: Int) -> ())!
  
  var code = ""
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
    loadSelectedLanguage()
    if let code = UserObject.userObject.introduce_code {
      self.code = code as String
      giftCodeLabel.text = self.code
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  func loadSelectedLanguage() {
    if let language: Array<String> = UserDefaults.standard.object(forKey: "AppleLanguages") as? Array<String> {
      print(language)
      if language.first == "vi-VN" {
        selectLanguage(index: 0)
      } else {
        selectLanguage(index: 1)
      }
    }
  }
  
  func saveSelectedLanguage() {
    UserDefaults.standard.set([selectedLanguage == 0 ? "vi-VN" : "en-VN"], forKey: "AppleLanguages")
    UserDefaults.standard.synchronize()
  }
  
  func selectLanguage(index: Int) {
    if selectedLanguage != index {
      selectedLanguage = index
      saveSelectedLanguage()
      if selectedLanguage == 0 {
        vnButton.backgroundColor = greenColor
        vnButton.setTitleColor(UIColor.white, for: .normal)
        vnButton.shadowColor = UIColor.black
        vnButton.shadowRadius = 2
        vnButton.shadowOpacity = 0.4
        vnButton.shadowOffset = CGSize(width: 0, height: 1)
        
        enButton.backgroundColor = UIColor.white
        enButton.setTitleColor(greenColor, for: .normal)
        enButton.shadowColor = UIColor.clear
        enButton.shadowRadius = 0
        enButton.shadowOpacity = 0
        enButton.shadowOffset = CGSize(width: 0, height: 0)
      } else {
        enButton.backgroundColor = greenColor
        enButton.setTitleColor(UIColor.white, for: .normal)
        enButton.shadowColor = UIColor.black
        enButton.shadowRadius = 2
        enButton.shadowOpacity = 0.4
        enButton.shadowOffset = CGSize(width: 0, height: 1)
        
        vnButton.backgroundColor = UIColor.white
        vnButton.setTitleColor(greenColor, for: .normal)
        vnButton.shadowColor = UIColor.clear
        vnButton.shadowRadius = 0
        vnButton.shadowOpacity = 0
        vnButton.shadowOffset = CGSize(width: 0, height: 0)
      }
      if selectLanguageAction != nil {
        selectLanguageAction(selectedLanguage)
      }
    }
  }
  
  @IBAction func vnButtonTouchUpInside(_ sender: Any) {
    selectLanguage(index: 0)
  }
  
  @IBAction func enButtonTouchUpInside(_ sender: Any) {
    selectLanguage(index: -1)
  }
  
  @IBAction func shareButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction()
    }
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if selectMenuAction != nil {
      switch indexPath.section {
      case 0:
        selectMenuAction(indexPath.row == 0 ? 0 : 2)
        break
      case 1:
        selectMenuAction(indexPath.row == 1 ? 4 : -1)
        break
      case 2:
        selectMenuAction(indexPath.row + 6)
        break
      default:
        break
      }
      
    }
  }
}
