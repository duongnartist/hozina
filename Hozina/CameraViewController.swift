//
//  CameraViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/14/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController, AVCapturePhotoCaptureDelegate {
  
  @IBOutlet weak var continueButton: UIButton!
  @IBOutlet weak var countLabel: UILabel!
  @IBOutlet weak var previewView: UIView!
  @IBOutlet weak var collectionView: UIView!
  
  @IBOutlet weak var add0Button: UIButton!
  @IBOutlet weak var add1Button: UIButton!
  @IBOutlet weak var add2Button: UIButton!
  @IBOutlet weak var add3Button: UIButton!
  
  @IBOutlet weak var close0Button: UIButton!
  @IBOutlet weak var close1Button: UIButton!
  @IBOutlet weak var close2Button: UIButton!
  @IBOutlet weak var close3Button: UIButton!
  
  @IBOutlet weak var photo0ImageView: BorderCornerImageView!
  @IBOutlet weak var photo1ImageView: BorderCornerImageView!
  @IBOutlet weak var photo2ImageView: BorderCornerImageView!
  @IBOutlet weak var photo3ImageView: BorderCornerImageView!
  
  var addButtons = [UIButton]()
  var closeButtons = [UIButton]()
  var photoImageViews = [BorderCornerImageView]()
  
  var images = [UIImage(), UIImage(), UIImage(), UIImage()]
  var filleds = [false, false, false, false]
  
  var currentIndex = 0
  var endIndex = 3
  
  var captureSession = AVCaptureSession()
  var sessionOutput = AVCapturePhotoOutput()
  var sessionOutputSetting = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecJPEG])
  var previewLayer = AVCaptureVideoPreviewLayer()
  var backCamera: AVCaptureDevice!
  
  var taked = false
  var isBack = true
  var doneAction: ((_ images: [UIImage], _ filleds: [Bool]) -> ())!
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
    
    addButtons = [add0Button, add1Button, add2Button, add3Button]
    closeButtons = [close0Button, close1Button, close2Button, close3Button]
    photoImageViews = [photo0ImageView, photo1ImageView, photo2ImageView, photo3ImageView]
    
    displayCount()
    loadCamera()
  }
  
  func loadCamera() {
    captureSession.stopRunning()
    previewLayer.removeFromSuperlayer()
    
    captureSession = AVCaptureSession()
    captureSession.sessionPreset = AVCaptureSessionPresetPhoto
    
    if isBack {
      backCamera = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back)
    } else {
      backCamera = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front)
    }
    
    var error: NSError?
    var input: AVCaptureDeviceInput!
    do {
      input = try AVCaptureDeviceInput(device: backCamera)
    } catch let error1 as NSError {
      error = error1
      input = nil
      print(error!.localizedDescription)
    }
    
    if error == nil && captureSession.canAddInput(input) {
      captureSession.addInput(input)
      sessionOutput = AVCapturePhotoOutput()
      if captureSession.canAddOutput(sessionOutput) {
        captureSession.addOutput(sessionOutput)
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = previewView.bounds
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.portrait
        previewView.layer.addSublayer(previewLayer)
        captureSession.startRunning()
      }
    }
  }
  
  func displayCount() {
    if currentIndex <= endIndex {
      countLabel.text = "\(currentIndex + 1) / \(endIndex + 1)"
    } else {
      countLabel.text = ""
    }
    var canNext = false
    for filled in filleds {
      if filled == true {
        canNext = filled
        break
      }
    }
    continueButton.isHidden = !canNext
    for i in 0 ..< images.count {
      closeButtons[i].isHidden = !filleds[i]
      addButtons[i].isHidden = filleds[i]
      photoImageViews[i].image = filleds[i] ? images[i] : nil
    }
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    previewLayer.frame = previewView.bounds
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  @IBAction func cancelButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func nextButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: {
      if self.doneAction != nil {
        self.doneAction(self.images, self.filleds)
      }
    })
  }
  
  @IBAction func takeButtonTouchUpInside(_ sender: Any) {
    if currentIndex <= endIndex {
      capture()
    }
  }
  
  func capture() {
    if (sessionOutput.connection(withMediaType: AVMediaTypeVideo)) != nil {
      let settings = AVCapturePhotoSettings()
      let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
      let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                           kCVPixelBufferWidthKey as String: 160,
                           kCVPixelBufferHeightKey as String: 160]
      settings.previewPhotoFormat = previewFormat
      sessionOutput.capturePhoto(with: settings, delegate: self)
    }
  }
  
  func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
    if let error = error {
      print(error.localizedDescription)
    }
    if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
      let dataProvider = CGDataProvider(data: imageData as CFData)
      let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
      let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.leftMirrored)
      if currentIndex < images.count {
        images[currentIndex] = image
        filleds[currentIndex] = true
        currentIndex += 1
        displayCount()
      }
    }
  }
  
  @IBAction func flashButtonTouchUpInside(_ sender: Any) {
    if backCamera.hasTorch {
      do {
        try backCamera.lockForConfiguration()
        if backCamera.torchMode == AVCaptureTorchMode.on {
          backCamera.torchMode = AVCaptureTorchMode.off
        } else {
          do {
            try backCamera.setTorchModeOnWithLevel(1.0)
          } catch {
            print(error)
          }
        }
        backCamera.unlockForConfiguration()
      } catch {
        print(error)
      }
    }
  }
  
  @IBAction func changeCameraButtonTouchUpInside(_ sender: Any) {
    isBack = !isBack
    loadCamera()
  }
  
  @IBAction func showCollectionButtonTouchUpInside(_ sender: Any) {
    collectionView.isHidden = !collectionView.isHidden
  }
  
  @IBAction func addImage0ButtonTouchUpInside(_ sender: Any) {
    addImage(at: 0)
  }
  
  @IBAction func addImage1ButtonTouchUpInside(_ sender: Any) {
    addImage(at: 1)
  }
  
  @IBAction func addImage2ButtonTouchUpInside(_ sender: Any) {
    addImage(at: 2)
  }
  
  @IBAction func addImage3ButtonTouchUpInside(_ sender: Any) {
    addImage(at: 3)
  }
  
  func addImage(at index: Int) {
    delImage(at: index)
  }
  
  @IBAction func delImage0ButtonTouchUpInside(_ sender: Any) {
    delImage(at: 0)
  }
  
  @IBAction func delImage1ButtonTouchUpInside(_ sender: Any) {
    delImage(at: 1)
  }
  
  @IBAction func delImage2ButtonTouchUpInside(_ sender: Any) {
    delImage(at: 2)
  }
  
  @IBAction func delImage3ButtonTouchUpInside(_ sender: Any) {
    delImage(at: 3)
  }
  
  func delImage(at index: Int) {
    currentIndex = index
    images[currentIndex] = UIImage()
    filleds[currentIndex] = false
    displayCount()
  }
}
