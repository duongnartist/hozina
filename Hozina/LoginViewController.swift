//
//  LoginViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/8/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZAlertController
import FBSDKLoginKit
import AccountKit

class LoginViewController: UIViewController {
  
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  fileprivate var accountKit = AKFAccountKit(responseType: .accessToken)
  fileprivate var pendingLoginViewController: AKFViewController?
  fileprivate var showAccountOnAppear = false
  
  var requesting = false
  var userObject = UserObject()
  
  override func loadView() {
    super.loadView()
    //    print("LoginViewController -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print("LoginViewController -> viewDidLoad")
    initUser()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print("LoginViewController -> viewWillAppear")
    if showAccountOnAppear {
      showAccountOnAppear = false
    } else if let viewController = pendingLoginViewController {
      prepareLoginViewController(viewController)
      if let viewController = viewController as? UIViewController {
        present(viewController, animated: animated, completion: nil)
        pendingLoginViewController = nil
      }
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print("LoginViewController -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print("LoginViewController -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print("LoginViewController -> viewDidDisappear")
  }
  
  deinit {
    //    print("LoginViewController -> deinit")
  }
  
  func initUser() {
    UserObject.userObject.open()
    if let phone_number = UserObject.userObject.phone_number, let password = UserObject.userObject.password {
      let phone = phone_number as String
      let pass = password as String
      if !phone.isEmpty && !pass.isEmpty {
        login(userObject: UserObject.userObject)
      }
    } else {
      if let facebookId = UserObject.userObject.facebook_id {
        let fb = facebookId as String
        if !fb.isEmpty {
          registerAndLoginFacebook(userObject: UserObject.userObject)
        }
      }
    }
  }
  
  func login(userObject: UserObject) {
    guard requesting else {
      show(requesting: &requesting)
      Network.login(userObject: userObject, response: {
        res in
        self.hide(requesting: &self.requesting)
        if res.code == 200 {
          self.onLoginSuccess()
        } else {
          if res.code == 401 {
            self.register(userObject: self.userObject)
          } else {
            self.onLoginFailure(message: res.message! as String)
          }
        }
      })
      return
    }
  }
  
  func register(userObject: UserObject) {
    guard requesting else {
      show(requesting: &requesting)
      Network.register(userObject: userObject, response: {
        res in
        self.hide(requesting: &self.requesting)
        if res.code == 200 {
          self.onLoginSuccess()
        } else {
          self.onLoginFailure(message: res.message! as String)
        }
      })
      return
    }
  }
  
  func registerAndLoginFacebook(userObject: UserObject) {
    guard requesting else {
      show(requesting: &requesting)
      Network.registerAndLoginFacebook(userObject: userObject, response: {
        res in
        self.hide(requesting: &self.requesting)
        if res.code == 200 {
          self.onLoginSuccess()
        } else {
          self.onLoginFailure(message: res.message! as String)
        }
      })
      return
    }
  }
  
  func onLoginSuccess() {
    view.window?.rootViewController = "MainViewController".viewController(storyboard: "Main")
  }
  
  func onLoginFailure(message: String) {
    EZAlertController.alert("alert_title".localized(), message: message, acceptMessage: "btn_ok".localized(), acceptBlock: {
      
    })
  }
  
  fileprivate func prepareLoginViewController(_ loginViewController: AKFViewController) {
    let theme: AKFTheme = AKFTheme.default()
    theme.headerBackgroundColor = UIColor(hexString: "#0abf5d")!
    theme.headerTextColor = UIColor(hexString: "#ffffff")!
    theme.iconColor = UIColor(hexString: "#0abf5d")!
    theme.inputTextColor = UIColor(hexString: "#0abf5d")!
    theme.statusBarStyle = .lightContent
    theme.textColor = UIColor(hexString: "#000000")!
    theme.titleColor = UIColor(hexString: "#000000")!
    theme.backgroundColor = UIColor(hexString: "#f2f1f1")!
    theme.inputBorderColor = UIColor(hexString: "#0abf5d")!
    theme.inputBackgroundColor = UIColor(hexString: "#ffffff")!
    theme.buttonTextColor = UIColor(hexString: "#ffffff")!
    theme.buttonBackgroundColor = UIColor(hexString: "#0abf5d")!
    theme.buttonBorderColor = UIColor(hexString: "#0abf5d")!
    theme.buttonDisabledBackgroundColor = UIColor(hexString: "#ffffff")!
    theme.buttonDisabledTextColor = UIColor(hexString: "#0abf5d")!
    theme.buttonDisabledBorderColor = UIColor(hexString: "#0abf5d")!
    loginViewController.setTheme(theme)
    loginViewController.delegate = self
  }
  
  @IBAction func loginButtonTouchUpInside(_ sender: Any) {
    guard requesting else {
      prepareUserObject(success: { _ in
        login(userObject: userObject)
//        if let viewController = accountKit.viewControllerForPhoneLogin(with: nil, state: nil) as? AKFViewController {
//          prepareLoginViewController(viewController)
//          if let viewController = viewController as? UIViewController {
//            present(viewController, animated: true, completion: nil)
//          }
//        }
      }, failure: { key, value in
        EZAlertController.alert("alert_title".localized(), message: value, acceptMessage: "btn_ok".localized(), acceptBlock: {
          switch key {
          case 0:
            self.usernameTextField.becomeFirstResponder()
            break
          case 1:
            self.passwordTextField.becomeFirstResponder()
            break
          default:
            break
          }
        })
      })
      return
    }
  }
  
  func prepareUserObject(success: (_ userObject: UserObject) -> (), failure: (_ key: Int, _ value: String) -> ()) {
    userObject.phone_number = usernameTextField.text! as NSString
    userObject.password = passwordTextField.text! as NSString
    if (userObject.phone_number?.length)! >= 10 {
      if (userObject.password?.length)! >= 8 {
        success(userObject)
      } else {
        failure(1, "invalid_pass".localized())
      }
    } else {
      failure(0, "invalid_phone".localized())
    }
  }
  
  @IBAction func facebookButtonTouchUpInside(_ sender: Any) {
    let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
    fbLoginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { result, error in
      if error == nil {
        let fbloginresult: FBSDKLoginManagerLoginResult = result!
        if fbloginresult.grantedPermissions != nil {
          if fbloginresult.grantedPermissions.contains("email") {
            self.getFBUserData()
            fbLoginManager.logOut()
          }
        }
      }
    }
  }
  
  func getFBUserData() {
    if (FBSDKAccessToken.current()) != nil {
      FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (_, result, error) -> () in
        if error == nil {
          let dict = result as! [String: AnyObject]
          let fbid = dict["id"] as? NSString
          let email = dict["email"] as? NSString
          let name = dict["name"] as? NSString
          let userObject = UserObject()
          userObject.facebook_id = fbid
          userObject.email = email
          userObject.name = name
          self.registerAndLoginFacebook(userObject: userObject)
        }
      })
    }
  }
}

extension LoginViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}

extension LoginViewController: AKFViewControllerDelegate {
  
  func viewController(_ viewController: UIViewController!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
    login(userObject: userObject)
  }
  
  func viewController(_ viewController: UIViewController!, didFailWithError error: Error!) {
    print("\(viewController) did fail with error: \(error)")
  }
}
