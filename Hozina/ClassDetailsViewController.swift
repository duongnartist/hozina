//
//  ClassDetailsViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/16/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import ImageSlideshow

class ClassDetailsViewController: UIViewController {
  
  var classObject: ClassObject!
  var commentData = [CommentObject]()
  
  @IBOutlet weak var slideShow: ImageSlideshow!
  @IBOutlet weak var tableView: UITableView!
  
  var kingfisherSources = [KingfisherSource]()
  
  var requesting = false
  var pageNumber = 1
  var isLoadMore = true
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 40
    
    tableView.es_addPullToRefresh {
      self.loadClassDetails()
      self.reloadClassComments()
    }
    
    tableView.es_addInfiniteScrolling {
      if self.isLoadMore {
        self.loadMoreClassComments()
      } else {
        self.tableView.stopLoading()
      }
    }
    
    displaySlideShow()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  func loadClassDetails() {
    Network.getClassDetails(classObject: classObject, response: { res in
      if res.isSuccess {
        self.classObject = res.result.first
        self.tableView.reloadData()
      }
    })
  }
  
  func reloadClassComments() {
    pageNumber = 1
    isLoadMore = true
    loadMoreClassComments()
  }
  
  func loadMoreClassComments() {
    guard requesting else {
      requesting = true
      Network.getListComments(classObject: classObject, pageNumber: pageNumber, response: { res in
        self.tableView.stopLoading()
        self.requesting = false
        if res.isSuccess {
          if self.pageNumber == 1 {
            self.commentData.removeAll()
          }
          if res.isEmpty() {
            self.isLoadMore = false
            self.tableView.es_noticeNoMoreData()
          } else {
            self.pageNumber += 1
            self.commentData += res.result
          }
          self.tableView.reloadSections([3], with: .automatic)
          self.tableView.scrollToRow(at: IndexPath(row: 0, section: 4), at: .bottom, animated: true)
        } else {
          self.isLoadMore = false
          self.tableView.es_noticeNoMoreData()
        }
      })
      return
    }
  }
  
  func displaySlideShow() {
    slideShow.backgroundColor = UIColor.white
    slideShow.slideshowInterval = 5.0
    slideShow.pageControlPosition = PageControlPosition.insideScrollView
    slideShow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
    slideShow.pageControl.pageIndicatorTintColor = UIColor.black
    slideShow.contentScaleMode = UIViewContentMode.scaleAspectFill
    slideShow.activityIndicator = DefaultActivityIndicator()
    slideShow.currentPageChanged = { _ in
      
    }
    let recognizer = UITapGestureRecognizer(target: self, action: #selector(HomeImageSlideCell.didTap))
    slideShow.addGestureRecognizer(recognizer)
    if kingfisherSources.isEmpty {
      if let classObject = self.classObject {
        if !classObject.photos.isEmpty {
          for image in classObject.photos {
            let kingfisherSource = KingfisherSource(urlString: image)
            kingfisherSources.append(kingfisherSource!)
          }
          slideShow.setImageInputs(kingfisherSources)
        }
      }
    }
  }
  
  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
}

extension ClassDetailsViewController: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 5
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      return 1
    case 1:
      return 1
    case 2:
      return 1
    case 3:
      return commentData.count
    case 4:
      return 1
    default:
      return 0
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.section {
    case 0:
      let cell = tableView.dequeueReusableCell(withIdentifier: "ClassInfoCell", for: indexPath) as! ClassInfoCell
      cell.display(classObject: classObject)
      return cell
      
    case 1:
      let cell = tableView.dequeueReusableCell(withIdentifier: "ClassStatCell", for: indexPath) as! ClassStatCell
      cell.display(classObject: classObject)
      return cell
      
    case 2:
      let cell = tableView.dequeueReusableCell(withIdentifier: "ClassActionCell", for: indexPath) as! ClassActionCell
      cell.tableView = tableView
      cell.display(classObject: classObject)
      return cell
      
    case 3:
      let cell = tableView.dequeueReusableCell(withIdentifier: "ClassCommentCell", for: indexPath) as! ClassCommentCell
      cell.display(commentObject: commentData.get(at: indexPath.row)!)
      return cell
      
    case 4:
      let cell = tableView.dequeueReusableCell(withIdentifier: "ClassNewCommentCell", for: indexPath) as! ClassNewCommentCell
      cell.display(classObject: classObject)
      return cell
      
    default:
      return UITableViewCell()
    }
  }
  
}
