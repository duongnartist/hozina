//
//  MainViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/8/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
  
  @IBOutlet weak var tab0View: UIView!
  @IBOutlet weak var tab1View: UIView!
  @IBOutlet weak var tab2View: UIView!
  @IBOutlet weak var tab3View: UIView!
  @IBOutlet weak var tab4View: UIView!
  
  var tabViews = [UIView]()
  
  override func loadView() {
    super.loadView()
    print("MainViewController -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    print("MainViewController -> viewDidLoad")
    tabViews = [tab0View, tab1View, tab2View, tab3View, tab4View]
    selectTabBar(index: 0)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    print("MainViewController -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    print("MainViewController -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print("MainViewController -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    print("MainViewController -> viewDidDisappear")
  }
  
  deinit {
    print("MainViewController -> deinit")
  }
  
  func selectTabBar(index: Int) {
    for i in 0 ..< tabViews.count {
      tabViews[i].isHidden = i != index
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    prepareEmbedTabBar(for: segue)
  }
  
  func prepareEmbedTabBar(for segue: UIStoryboardSegue) {
    if segue.identifier == "EmbedTabBar" {
      if let tabBarViewController: TabBarViewController = segue.destination as? TabBarViewController {
        tabBarViewController.selectTabBarIndexAction = { index in
          self.selectTabBar(index: index)
        }
      }
    }
  }
}
