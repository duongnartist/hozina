//
//  GiftCodeCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class GiftCodeCell: UICollectionViewCell {
  
  @IBOutlet weak var codeLabel: UILabel!
  
  func display(userObject: UserObject) {
    if let value = userObject.introduce_code {
      codeLabel.text = "\(value)"
    } else {
      codeLabel.text = ""
    }
  }
}
