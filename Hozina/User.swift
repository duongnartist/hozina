//
//  UserData.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class UserResponse: BaseResponse {
  var result: UserObject?
}

class UserObject: EVObject {
  
  static var userObject = UserObject()
  static var rememberMe = true
  
  var id: NSNumber? = 0
  var _id: Int {
    return id?.intValue ?? 0
  }
  
  var name: NSString? = ""
  var _name: String {
    return name as String? ?? ""
  }
  
  var facebook_id: NSString? = ""
  var email: NSString? = ""
  var photo: NSString? = ""
  var token_firebase: NSString? = ""
  var remember_token: NSString? = ""
  var is_admin: NSNumber? = 0
  var is_active: NSNumber? = 0
  var is_suspend: NSNumber? = 0
  var phone_number: NSString? = ""
  var address: NSString? = ""
  var description_vi: NSString? = ""
  var device_type: NSNumber? = 2
  var created_at: NSString? = ""
  var updated_at: NSString? = ""
  var ApiToken: NSString? = ""
  var password: NSString? = ""
  var birthday: NSString? = ""
  var introduce_code: NSString? = ""
  var number_favorite: NSNumber? = 0
  var number_enjoying: NSNumber? = 0
  var number_share: NSNumber? = 0
  
  var is_favorite: NSNumber? = 0

  func isFavorite() -> Bool {
    if let value = self.is_favorite {
      if (value as! Int) > 0 {
        return true
      }
      return false
    }
    return false
  }
  
  func clone() -> UserObject {
    let userObject = UserObject()
    userObject.name = name
    userObject.birthday = birthday
    userObject.phone_number = phone_number
    userObject.email = email
    userObject.address = address
    userObject.description_vi = description_vi
    userObject.photo = photo
    userObject.password = password
    return userObject
  }
  
  func getAvatar() -> String {
    if let photo = self.photo {
      let photos = (photo as String).split(",")
      if !photos.isEmpty {
        return photos.first!
      }
    }
    return ""
  }
  
  func save() {
    if UserObject.rememberMe {
      print("+ Save User Data")
      let std = UserDefaults.standard
      std.set(email, forKey: "email")
      print("\t- email       : \(String(describing: email))")
      std.set(facebook_id, forKey: "facebook_id")
      print("\t- facebook_id : \(String(describing: facebook_id))")
      std.set(id, forKey: "id")
      print("\t- id          : \(String(describing: id))")
      std.set(phone_number, forKey: "phone_number")
      print("\t- phone_number: \(String(describing: phone_number))")
      std.set(password, forKey: "password")
      print("\t- password    : \(String(describing: password))")
    } else {
      print("+ You never check remember me!")
    }
  }
  
  func open() {
    print("+ Open User Data")
    let std = UserDefaults.standard
    email = std.object(forKey: "email") as? NSString ?? ""
    print("\t- email       : \(email!)")
    facebook_id = std.object(forKey: "facebook_id") as? NSString ?? ""
    print("\t- facebook_id : \(facebook_id!)")
    id = std.object(forKey: "id") as? NSNumber ?? 0
    print("\t- id          : \(id!)")
    phone_number = std.object(forKey: "phone_number") as? NSString ?? ""
    print("\t- phone_number: \(phone_number!)")
    password = std.object(forKey: "password") as? NSString ?? ""
    print("\t- password    : \(password!)")
    if Int(id!) > 0 {
      UserObject.rememberMe = true
    }
  }
  
  func clear() {
    email = ""
    facebook_id = ""
    id = 0
    phone_number = ""
    password = ""
    print("+ Clear User Data")
    let std = UserDefaults.standard
    std.set(email, forKey: "email")
    print("\t- email       : \(String(describing: email))")
    std.set(facebook_id, forKey: "facebook_id")
    print("\t- facebook_id : \(String(describing: facebook_id))")
    std.set(id, forKey: "id")
    print("\t- id          : \(String(describing: id))")
    std.set(phone_number, forKey: "phone_number")
    print("\t- phone_number: \(String(describing: phone_number))")
    std.set(password, forKey: "password")
    print("\t- password    : \(String(describing: password))")
  }
}
