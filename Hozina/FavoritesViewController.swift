//
//  FavoritesViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/11/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class FavoritesViewController: UIViewController {
  
  var classData = [ClassObject]()
  var classCellSize: CGSize!
  var titleSize: CGSize!
  var pageNumber = 1
  var isLoadMore = true
  var isRequesting = false
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
    titleSize = CGSize(width: ez.screenWidth, height: 40)
    
    let width = (ez.screenWidth - 30) / 2
    let height = width * 200 / 150
    classCellSize = CGSize(width: width, height: height)
    
    collectionView.es_addPullToRefresh {
      self.reloadData()
    }
    
    collectionView.es_addInfiniteScrolling {
      self.loadMoreData()
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
    collectionView.es_startPullToRefresh()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  func reloadData() {
    pageNumber = 1
    isLoadMore = true
    loadMoreData()
  }
  
  func loadMoreData() {
    if isLoadMore {
      guard isRequesting else {
        isRequesting = true
        Network.getListClassByUser(userObject: UserObject.userObject, categoryObject: CategoryObject(), classType: 3, pageNumber: pageNumber, response: { res in
          self.isRequesting = false
          self.collectionView.stopLoading()
          if self.pageNumber == 1 {
            self.classData.removeAll()
          }
          if res.isSuccess {
            if res.isEmpty() {
              self.isLoadMore = false
              self.collectionView.es_noticeNoMoreData()
            } else {
              if self.pageNumber == 1 {
                self.collectionView.es_resetNoMoreData()
              }
              self.pageNumber += 1
              self.classData += res.result
            }
          } else {
            self.isLoadMore = false
            self.collectionView.es_noticeNoMoreData()
          }
          self.collectionView.reloadData()
        })
        return
      }
    } else {
      collectionView.stopLoading()
    }
  }
  
}

extension FavoritesViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 2
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    switch section {
    case 0:
      return 1
    case 1:
      return classData.count
    default:
      return 0
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    switch indexPath.section {
    case 0:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TitleCell", for: indexPath) as! TitleCell
      return cell
      
    case 1:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClassCell", for: indexPath) as! ClassCell
      cell.navigationController = navigationController
      cell.display(classObject: classData.get(at: indexPath.row)!)
      return cell
      
    default:
      return UICollectionViewCell()
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    switch indexPath.section {
    case 0:
      return titleSize
      
    case 1:
      return classCellSize
      
    default:
      return CGSize(width: 0, height: 0)
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if indexPath.section > 0 {
      let classObject = classData.get(at: indexPath.row)
      if let navigationController: UINavigationController = "ClassDetailsNavigationViewController".viewController(storyboard: "Main") as? UINavigationController {
        if let classDetailsViewController: ClassDetailsViewController = navigationController.topViewController as? ClassDetailsViewController {
          classDetailsViewController.classObject = classObject
          present(navigationController, animated: true, completion: nil)
        }
      }
    }
  }
}
