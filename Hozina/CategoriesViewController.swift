//
//  CategoriesViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/11/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController {
  
  var categoryData = [CategoryObject]()
  var categoryObject = CategoryObject()
  var selectedCategory: ((_ categoryObject: CategoryObject) -> ())!
  
  override func loadView() {
    super.loadView()
    //    print("CategoriesViewController -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print("CategoriesViewController -> viewDidLoad")
    for i in 0 ..< categoryData.count {
      categoryData[i].isSelected = categoryData[i].id == categoryObject.id
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print("CategoriesViewController -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print("CategoriesViewController -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print("CategoriesViewController -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print("CategoriesViewController -> viewDidDisappear")
  }
  
  deinit {
    //    print("CategoriesViewController -> deinit")
  }
  
  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
}

extension CategoriesViewController: UITableViewDataSource, UITableViewDelegate {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return categoryData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as! CategoryTableViewCell
    cell.display(categoryObject: categoryData.get(at: indexPath.row)!)
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    for i in 0 ..< categoryData.count {
      categoryData[i].isSelected = i == indexPath.row
    }
    tableView.reloadData()
    navigationController?.popViewController(animated: true)
    if selectedCategory != nil {
      selectedCategory(categoryData[indexPath.row])
    }
  }
}
