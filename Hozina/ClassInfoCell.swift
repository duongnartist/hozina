//
//  ClassInfoCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/16/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ClassInfoCell: UITableViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var postedDateLabel: UILabel!
  @IBOutlet weak var categoryLabel: UILabel!
  @IBOutlet weak var openDateLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var teacherLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var phoneLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  
  let grayColor = UIColor(hexString: "#898989")
  let blackColor = UIColor.black
  
  func display(classObject: ClassObject) {
    if let value = classObject.user_photo {
      if let url = URL(string: value as String) {
        avatarImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Icon_Avatar_Default"))
      } else {
        avatarImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
      }
    } else {
      avatarImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
    }
    
    if let value = classObject.name {
      nameLabel.text = value as String
    } else {
      nameLabel.text = ""
    }
    
    if let value = classObject.created_at {
      postedDateLabel.text = (value as String).ddMMyyy
    } else {
      postedDateLabel.text = Date().ddMMyyyy
    }
    
    if let value = classObject.category_id {
      let categoryObject = CategoryObject.categoryById(id: value)
      let valueString = categoryObject.name! as String
      let attributedText = NSMutableAttributedString(string: "class_category".localized() + valueString)
      attributedText.setColorForText("class_category".localized(), with: grayColor!)
      attributedText.setColorForText(valueString, with: blackColor)
      categoryLabel.attributedText = attributedText
    } else {
      categoryLabel.text = "class_category".localized()
    }
    
    if let value = classObject.start_date {
      let valueString = (value as String).ddMMyyy
      let attributedText = NSMutableAttributedString(string: "class_start_date".localized() + valueString)
      attributedText.setColorForText("class_start_date".localized(), with: grayColor!)
      attributedText.setColorForText(valueString, with: blackColor)
      openDateLabel.attributedText = attributedText
    } else {
      openDateLabel.text = "class_start_date".localized()
    }
    
    if let value = classObject.price {
      let valueString = value.price
      let attributedText = NSMutableAttributedString(string: "class_price".localized() + valueString)
      attributedText.setColorForText("class_price".localized(), with: grayColor!)
      attributedText.setColorForText(valueString, with: blackColor)
      priceLabel.attributedText = attributedText
    } else {
      priceLabel.text = "class_price".localized()
    }
    
    if let value = classObject.teacher {
      let valueString = value as String
      let attributedText = NSMutableAttributedString(string: "class_teacher".localized() + valueString)
      attributedText.setColorForText("class_teacher".localized(), with: grayColor!)
      attributedText.setColorForText(valueString, with: blackColor)
      teacherLabel.attributedText = attributedText
    } else {
      teacherLabel.text = "class_teacher".localized()
    }
    
    if let value = classObject.address {
      let valueString = value as String
      let attributedText = NSMutableAttributedString(string: "class_address".localized() + valueString)
      attributedText.setColorForText("class_address".localized(), with: grayColor!)
      attributedText.setColorForText(valueString, with: blackColor)
      addressLabel.attributedText = attributedText
    } else {
      addressLabel.text = "class_address".localized() + "\n"
    }
    
    if let value = classObject.phone_number {
      let valueString = value as String
      let attributedText = NSMutableAttributedString(string: "class_phone".localized() + valueString)
      attributedText.setColorForText("class_phone".localized(), with: grayColor!)
      attributedText.setColorForText(valueString, with: blackColor)
      phoneLabel.attributedText = attributedText
    } else {
      phoneLabel.text = "class_phone".localized()
    }
    
    if let value = classObject.description_vi {
      let valueString = value as String
      let attributedText = NSMutableAttributedString(string: "class_description".localized() + valueString)
      attributedText.setColorForText("class_description".localized(), with: grayColor!)
      attributedText.setColorForText(valueString, with: blackColor)
      descriptionLabel.attributedText = attributedText
    } else {
      descriptionLabel.text = "class_description".localized()
    }
  }
}
