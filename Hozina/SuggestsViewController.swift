//
//  SuggestsViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class SuggestsViewController: UIViewController {
  
  var appData = [AppObject]()
  
  override func loadView() {
    super.loadView()
//    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    print(" -> viewDidLoad")
    for i in 0 ... 20 {
      let appObject = AppObject()
      appObject.name = "Name \(i)" as NSString
      appObject.detail = "Detail \(i)" as NSString
      appObject.photo = "http://www.wonderslist.com/wp-content/uploads/2016/07/Most-Beautiful-Girls-India.jpg" as NSString
      appData.append(appObject)
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
//    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
//    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
//    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
//    print(" -> viewDidDisappear")
  }
  
  deinit {
//    print(" -> deinit")
  }
  
  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
  
}

extension SuggestsViewController: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return appData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "AppCell", for: indexPath) as! AppCell
    cell.display(appObject: appData.get(at: indexPath.row)!)
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
  }
}
