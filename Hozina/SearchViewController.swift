//
//  SearchViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/9/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
  
  @IBOutlet weak var searchTextField: UITextField!
  
  override func loadView() {
    super.loadView()
    //    print("SearchViewController -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print("SearchViewController -> viewDidLoad")
    
    searchTextField.make(cornerRadius: 6)
    searchTextField.make(borderWidth: 0.5, borderColor: UIColor.white.cgColor)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print("SearchViewController -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print("SearchViewController -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print("SearchViewController -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print("SearchViewController -> viewDidDisappear")
  }
  
  deinit {
    //    print("SearchViewController -> deinit")
  }
  
  @IBAction func cancelButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return UITableViewCell()
  }
}
