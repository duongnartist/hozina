//
//  SystemNotificationCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/9/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import Kingfisher

class SystemNotificationCell: UITableViewCell {
  
  @IBOutlet weak var imageImageView: BorderCornerImageView!
  @IBOutlet weak var avatarImageView: BorderCornerImageView!
  @IBOutlet weak var activityLabel: UILabel!
  @IBOutlet weak var dateTimeLabel: UILabel!
  
  func display(systemNotification: SystemNotification) {
    if let avatar = systemNotification.avatar {
      avatarImageView.kf.setImage(with: URL(string: avatar as String))
    }
    if let image = systemNotification.image {
      imageImageView.kf.setImage(with: URL(string: image as String))
    }
    if let name = systemNotification.name {
      activityLabel.text = name as String
    }
    if let dateTime = systemNotification.dateTime {
      dateTimeLabel.text = dateTime as String
    }
    if let isRead = systemNotification.read {
      
    }
  }
  
}
