//
//  CategoryCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import Kingfisher

class CategoryCell: UICollectionViewCell {
  
  @IBOutlet weak var backView: BorderCornerView!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  
  func display(categoryObject: CategoryObject) {
    if let image = categoryObject.photo_no2 {
      if let url = URL(string: image as String) {
        avatarImageView.kf.setImage(with: url)
      } else {
        avatarImageView.image = #imageLiteral(resourceName: "Icon_Cell_Location")
      }
    } else {
      avatarImageView.image = #imageLiteral(resourceName: "Icon_Cell_Location")
    }
    if let title = categoryObject.name {
      titleLabel.text = title as String
    }
  }
}
