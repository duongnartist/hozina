//
//  UserViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/11/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import ESPullToRefresh

class UserViewController: UIViewController {
  
  @IBOutlet weak var menuButton: UIButton!
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var collectionView: UICollectionView!
  var classData = [ClassObject]()
  var classCellSize: CGSize!
  var profileSize: CGSize!
  var titleSize: CGSize!
  var fbAuthSize: CGSize!
  
  var requesting = false
  var pageNumber = 1
  var isLoadMore = true
  
  var userObject = UserObject.userObject
  var isMyProfile = true
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
    
    profileSize = CGSize(width: ez.screenWidth, height: isMyProfile ? 290 : 320)
    titleSize = CGSize(width: ez.screenWidth, height: 56)
    fbAuthSize = CGSize(width: ez.screenWidth, height: 56)
    
    let width = (ez.screenWidth - 30) / 2
    let height = width * 200 / 150
    classCellSize = CGSize(width: width, height: height)
    
    backButton.isHidden = isMyProfile
    
    collectionView.es_addPullToRefresh {
      self.reloadUserProfile()
    }
    
    collectionView.es_addInfiniteScrolling {
      self.loadMoreUserProfile()
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
    reloadUserProfile()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  func reloadUserProfile() {
    self.pageNumber = 1
    self.isLoadMore = true
    self.getUserProfile()
    if isMyProfile {
      self.getMyClasses()
    } else {
      self.getSharedClassesByUser()
    }
  }
  
  func loadMoreUserProfile() {
    if self.isLoadMore {
      if isMyProfile {
        self.getMyClasses()
      } else {
        self.getSharedClassesByUser()
      }
    } else {
      self.collectionView.stopLoading()
    }
  }
  
  func getUserProfile() {
    Network.getUserProfile(userObject: self.userObject, response: { res in
      if res.isSuccess {
        if self.isMyProfile {
          self.userObject = UserObject.userObject
        }
        self.collectionView.reloadData()
      } else {
      }
    })
  }
  
  func getMyClasses() {
    guard requesting else {
      if isLoadMore {
        requesting = true
        Network.getListClassByUser(userObject: self.userObject, categoryObject: CategoryObject(), classType: 1, pageNumber: pageNumber, response: { res in
          self.requesting = false
          self.collectionView.stopLoading()
          if self.pageNumber == 1 {
            self.classData.removeAll()
          }
          if res.isSuccess {
            if res.isEmpty() {
              self.isLoadMore = false
              self.collectionView.es_noticeNoMoreData()
            } else {
              if self.pageNumber == 1 {
                self.collectionView.es_resetNoMoreData()
              }
              self.pageNumber += 1
              self.classData += res.result
            }
          } else {
            self.isLoadMore = false
            self.collectionView.es_noticeNoMoreData()
          }
          self.collectionView.reloadData()
        })
      } else {
        self.collectionView.es_noticeNoMoreData()
      }
      return
    }
  }
  
  func getSharedClassesByUser() {
    guard requesting else {
      if isLoadMore {
        requesting = true
        Network.getListClassByUser(userObject: self.userObject, categoryObject: CategoryObject(), classType: 2, pageNumber: pageNumber, response: { res in
          self.requesting = false
          self.collectionView.stopLoading()
          if self.pageNumber == 1 {
            self.classData.removeAll()
          }
          if res.isSuccess {
            if res.isEmpty() {
              self.isLoadMore = false
              self.collectionView.es_noticeNoMoreData()
            } else {
              if self.pageNumber == 1 {
                self.collectionView.es_resetNoMoreData()
              }
              self.pageNumber += 1
              self.classData += res.result
            }
          } else {
            self.isLoadMore = false
            self.collectionView.es_noticeNoMoreData()
          }
          self.collectionView.reloadData()
        })
      } else {
        self.collectionView.es_noticeNoMoreData()
      }
      return
    }
  }
  
  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
  
}

extension UserViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return isMyProfile ? 6 : 4
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    switch section {
    case 0:
      return 1
    case 1:
      return 1
    case 2:
      return 1
    case 3:
      return isMyProfile ? 1 : classData.count
    case 4:
      return isMyProfile ? 1 : 0
    case 5:
      return isMyProfile ? classData.count : 0
    default:
      return 0
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    switch indexPath.section {
    case 0:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
      if (userObject.id as! Int) > 0 {
        cell.display(userObject: userObject, isMe: isMyProfile)
      }
      return cell
      
    case 1:
      if isMyProfile {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DescriptionCell", for: indexPath) as! DescriptionCell
        if let description = userObject.description_vi {
          cell.display(text: description as String)
        }
        return cell
      } else {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FacebookAuthCell", for: indexPath) as! FacebookAuthCell
        return cell
      }
      
    case 2:
      if isMyProfile {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FacebookAuthCell", for: indexPath) as! FacebookAuthCell
        return cell
      } else {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TitleCell", for: indexPath) as! TitleCell
        cell.display(text: "profile_my_shared_collection".localized())
        return cell
      }
      
    case 3:
      if isMyProfile {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCodeCell", for: indexPath) as! GiftCodeCell
        if (userObject.id as! Int) > 0 {
          cell.display(userObject: userObject)
        }
        return cell
      } else {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClassCell", for: indexPath) as! ClassCell
        cell.display(classObject: classData.get(at: indexPath.row)!)
        return cell
      }
      
    case 4:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TitleCell", for: indexPath) as! TitleCell
      cell.display(text: "profile_my_collection".localized())
      return cell
      
    case 5:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClassCell", for: indexPath) as! ClassCell
      cell.display(classObject: classData.get(at: indexPath.row)!)
      return cell
      
    default:
      return UICollectionViewCell()
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    switch indexPath.section {
    case 0:
      return profileSize
      
    case 1:
      return fbAuthSize
      
    case 2:
      return fbAuthSize
      
    case 3:
      return isMyProfile ? fbAuthSize : classCellSize
      
    case 4:
      return titleSize
      
    case 5:
      return classCellSize
      
    default:
      return CGSize(width: 0, height: 0)
    }
  }
}
