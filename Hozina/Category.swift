//
//  Category.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class CategoryResponse: BaseResponse {
  var result: [CategoryObject] = []
}

class CategoryObject: EVObject {
  
  static var categoryData = [CategoryObject]()
  static var nearCategory = CategoryObject()
  
  var id: NSNumber? = 0
  var name: NSString? = ""
  var photo_no1: NSString? = ""
  var photo_no2: NSString? = ""
  var description_vi: NSString? = ""
  var name_eng: NSString? = ""
  var description_eng: NSString? = ""
  var created_at: NSString? = ""
  var updated_at: NSString? = ""
  
  var isSelected: Bool = false
  
  static func categoryById(id: NSNumber) -> CategoryObject {
    for categoryObject in categoryData {
      if categoryObject.id == id {
        return categoryObject
      }
    }
    return CategoryObject()
  }
}
