//
//  ClassCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ClassCell: UICollectionViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var userImageView: UIImageView!
  @IBOutlet weak var likeLabel: UILabel!
  @IBOutlet weak var userLabel: UILabel!
  
  var navigationController: UINavigationController!
  var classObject: ClassObject!
  
  func display(classObject: ClassObject) {
    self.classObject = classObject
    if let _ = classObject.photo {
      if !classObject.photos.isEmpty {
        let photoUlr = classObject.photos.first
        if let url = URL(string: photoUlr!) {
          avatarImageView.kf.setImage(with: url)
        } else {
          avatarImageView.image = nil
        }
      } else {
        avatarImageView.image = nil
      }
    } else {
      avatarImageView.image = nil
    }
    if let value = classObject.name {
      nameLabel.text = value as String
    } else {
      nameLabel.text = ""
    }
    if let value = classObject.address {
      addressLabel.text = value as String
    } else {
      addressLabel.text = ""
    }
    if let value = classObject.number_like {
      likeLabel.text = "\(value)"
    } else {
      userLabel.text = ""
    }
    if let value = classObject.user_name {
      userLabel.text = value as String
    } else {
      userLabel.text = ""
    }
    if let value = classObject.user_photo {
      if let url = URL(string: value as String) {
        userImageView.kf.setImage(with: url)
      } else {
        userImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
      }
    } else {
      userImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
    }
  }
  
  @IBAction func showProfile(_ sender: UIButton) {
    if navigationController != nil {
      if self.classObject.user_id != UserObject.userObject.id {
        if let userViewController: UserViewController = "UserViewController".viewController(storyboard: "Main") as? UserViewController {
          let userObject = UserObject()
          userObject.id = classObject.user_id
          userObject.name = classObject.user_name
          userObject.photo = classObject.user_photo
          userViewController.isMyProfile = false
          userViewController.userObject = userObject
          navigationController.pushViewController(userViewController, animated: true)
        }
      } else {
        NotificationCenter.default.post(name: NSNotification.Name("ShowMyProfile"), object: nil)
      }
    }
  }
  
}
