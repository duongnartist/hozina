//
//  CategoryTableViewCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/11/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var statusImageView: UIImageView!
  
  func display(categoryObject: CategoryObject) {
    if let value = categoryObject.photo_no1 {
      if let url = URL(string: value as String) {
        avatarImageView.kf.setImage(with: url)
      }
    }
    if let value = categoryObject.name {
      nameLabel.text = value as String
    }
    statusImageView.image = categoryObject.isSelected ? #imageLiteral(resourceName: "Icon_Circle_Checked") : #imageLiteral(resourceName: "Icon_Circle_Unchecked")
  }
  
}
