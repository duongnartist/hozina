//
//  ShareViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/17/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController {
  
  var shareAction: ((_ index: Int) -> ())!
  
  override func loadView() {
    super.loadView()
    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    print(" -> viewDidLoad")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    print(" -> viewDidDisappear")
  }
  
  deinit {
    print(" -> deinit")
  }
  
  @IBAction func messageButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction(0)
    }
  }
  
  @IBAction func emailButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction(1)
    }
  }
  
  @IBAction func facebookButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction(2)
    }
  }
  
  @IBAction func messagerButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction(3)
    }
  }
  
  @IBAction func twitterButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction(4)
    }
  }
  
  @IBAction func whatsappButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction(5)
    }
  }
  
  @IBAction func googleButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction(6)
    }
  }
  
  @IBAction func linkedinButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction(7)
    }
  }
  
  @IBAction func moreButtonTouchUpInside(_ sender: Any) {
    if shareAction != nil {
      shareAction(8)
    }
  }
  
  @IBAction func cancelButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
}
