//
//  ClassesByCategoryViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class ClassesByCategoryViewController: UIViewController {
  
  var subCategoryCellSize: CGSize!
  var classCellSize: CGSize!
  
  @IBOutlet weak var addButton: BorderCornerButton!
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var categoryAvatarImageView: UIImageView!
  @IBOutlet weak var categoryNameButton: UIButton!
  @IBOutlet weak var collectionView: UICollectionView!
  
  var categoryData = [CategoryObject]()
  var classData = [ClassObject]()
  var subCategoryData = [SubCategoryObject]()
  var selectedSubCategory = SubCategoryObject()
  var categoryObject: CategoryObject!
  var requesting = false
  
  var pageNumber = 1
  var isLoadMore = true
  
  var isNoSubCategory = true
  
  override func loadView() {
    super.loadView()
    //    print("ClassesByCategoryViewController -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print("ClassesByCategoryViewController -> viewDidLoad")
    
    subCategoryCellSize = CGSize(width: ez.screenWidth, height: 150)
    let width = (ez.screenWidth - 30) / 2
    let height = width * 200 / 150
    classCellSize = CGSize(width: width, height: height)
    
    if categoryObject != nil {
      addButton.isHidden = true
      displaySelectedCategory()
      fetchSubCategoriesData()
    } else {
      backButton.isHidden = true
      NotificationCenter.default.addObserver(self, selector: #selector(fetchedCategoryData), name: NSNotification.Name("FetchedCategoryData"), object: nil)
    }
    
    collectionView.es_addPullToRefresh {
      self.reloadData()
    }
    
    collectionView.es_addInfiniteScrolling {
      self.loadMoreData()
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print("ClassesByCategoryViewController -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print("ClassesByCategoryViewController -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print("ClassesByCategoryViewController -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print("ClassesByCategoryViewController -> viewDidDisappear")
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name("FetchedCategoryData"), object: nil)
  }
  
  deinit {
    //    print("ClassesByCategoryViewController -> deinit")
  }
  
  func reloadData() {
    self.pageNumber = 1
    self.isLoadMore = true
    if self.isNoSubCategory {
      self.fetchClassesByCategory()
    } else {
      self.fetchClasses(by: self.selectedSubCategory)
    }
  }
  
  func loadMoreData() {
    if self.isLoadMore {
      if self.isNoSubCategory {
        self.fetchClassesByCategory()
      } else {
        self.fetchClasses(by: self.selectedSubCategory)
      }
    } else {
      self.collectionView.es_noticeNoMoreData()
      self.collectionView.stopLoading()
    }
  }
  
  func fetchedCategoryData(notification: Notification) {
    if let res: CategoryResponse = notification.object as? CategoryResponse {
      categoryData = res.result
    }
    if !categoryData.isEmpty {
      categoryData.first?.isSelected = true
      categoryObject = categoryData.first
      displaySelectedCategory()
      if !isNoSubCategory {
        fetchSubCategoriesData()
      } else {
        fetchClassesByCategory()
      }
    }
  }
  
  func displaySelectedCategory() {
    if categoryObject != nil {
      if let value = categoryObject.name {
        categoryNameButton.setTitle(value as String, for: .normal)
      } else {
        categoryNameButton.setTitle("", for: .normal)
      }
      if let value = categoryObject.photo_no2 {
        categoryAvatarImageView.kf.setImage(with: URL(string: value as String))
      } else {
        categoryAvatarImageView.image = nil
      }
    }
  }
  
  func fetchSubCategoriesData() {
    guard requesting else {
      if categoryObject != nil {
        classData.removeAll()
        subCategoryData.removeAll()
        collectionView.reloadData()
        requesting = true
        Network.getSubCategory(categoryObject: categoryObject, response: { res in
          self.requesting = false
          if res.isSuccess {
            self.subCategoryData = res.result
            self.collectionView.reloadData()
            if !self.subCategoryData.isEmpty {
              self.isLoadMore = true
              self.pageNumber = 1
              self.selectedSubCategory = self.subCategoryData.first!
              self.fetchClasses(by: self.selectedSubCategory)
            }
          } else {
            
          }
        })
      }
      return
    }
  }
  
  func fetchClassesByCategory() {
    guard requesting else {
      if isLoadMore {
        requesting = true
        Network.getListClass(categoryObject: categoryObject, page_number: pageNumber, response: { res in
          self.requesting = false
          self.collectionView.stopLoading()
          if self.pageNumber == 1 {
            self.classData.removeAll()
          }
          if res.isSuccess {
            if res.isEmpty() {
              self.isLoadMore = false
              self.collectionView.es_noticeNoMoreData()
            } else {
              if self.pageNumber == 1 {
                self.collectionView.es_resetNoMoreData()
              }
              self.pageNumber += 1
              self.classData += res.result
            }
          } else {
            self.isLoadMore = false
            self.collectionView.es_noticeNoMoreData()
          }
          self.collectionView.reloadData()
        })
      } else {
        collectionView.es_noticeNoMoreData()
      }
      return
    }
  }
  
  func fetchClasses(by subCategoryObject: SubCategoryObject) {
    guard requesting else {
      if isLoadMore {
        requesting = true
        Network.getListClass(subCategoryObject: subCategoryObject, pageNumber: pageNumber, response: { res in
          self.requesting = false
          self.collectionView.stopLoading()
          if self.pageNumber == 1 {
            self.classData.removeAll()
          }
          if res.isSuccess {
            if res.isEmpty() {
              self.isLoadMore = false
            } else {
              self.pageNumber += 1
              self.classData += res.result
            }
          } else {
            self.isLoadMore = false
          }
          self.collectionView.reloadData()
        })
      }
      return
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
  }
  
  @IBAction func categoryButtonTouchUpInside(_ sender: Any) {
    if let categoriesViewController: CategoriesViewController = "CategoriesViewController".viewController(storyboard: "Main") as? CategoriesViewController {
      categoriesViewController.categoryData = categoryData
      categoriesViewController.categoryObject = categoryObject
      categoriesViewController.selectedCategory = { categoryObject in
        self.categoryObject = categoryObject
        self.displaySelectedCategory()
        if self.isNoSubCategory {
          self.isLoadMore = true
          self.pageNumber = 1
          self.fetchClassesByCategory()
        } else {
          self.fetchSubCategoriesData()
        }
      }
      navigationController?.pushViewController(categoriesViewController, animated: true)
    }
  }
  
  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
}

extension ClassesByCategoryViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}

extension ClassesByCategoryViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return isNoSubCategory ? 1 : 2
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return (isNoSubCategory ? classData.count : (section == 0 ? 1 : classData.count))
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if !isNoSubCategory && indexPath.section == 0 {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryListCell", for: indexPath) as! SubCategoryListCell
      cell.display(data: subCategoryData)
      cell.selectSubCategoryAction = { index in
        let subCategoryObject = self.subCategoryData.get(at: index)
        self.isLoadMore = true
        self.pageNumber = 1
        self.fetchClasses(by: subCategoryObject!)
      }
      return cell
    }
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClassCell", for: indexPath) as! ClassCell
    cell.navigationController = navigationController
    cell.display(classObject: classData.get(at: indexPath.row)!)
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    if !isNoSubCategory && indexPath.section == 0 {
      return subCategoryCellSize
    }
    return classCellSize
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let section = isNoSubCategory ? 0 : 1
    if indexPath.section == section {
      let classObject = classData.get(at: indexPath.row)
      if let navigationController: UINavigationController = "ClassDetailsNavigationViewController".viewController(storyboard: "Main") as? UINavigationController {
        if let classDetailsViewController: ClassDetailsViewController = navigationController.topViewController as? ClassDetailsViewController {
          classDetailsViewController.classObject = classObject
          present(navigationController, animated: true, completion: nil)
        }
      }
    }
  }
}
