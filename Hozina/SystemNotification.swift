//
//  SystemNotification.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/9/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class SystemNotification: EVObject {
 
  var avatar: NSString?
  var _avatar: String {
    return avatar as String? ?? ""
  }
  
  var name: NSString?
  var _name: String {
    return name as String? ?? ""
  }
  
  var activity: NSString?
  var _activity: String {
    return activity as String? ?? ""
  }
  
  var dateTime: NSString?
  var _dateTime: String {
    return dateTime as String? ?? ""
  }
  
  var image: NSString?
  var _image: String {
    return image as String? ?? ""
  }
  
  var read: Bool?
  var _read: Bool {
    return read as Bool? ?? false
  }
  
}
