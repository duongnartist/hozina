//
//  Class.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class ClassResponse: BaseResponse {
  var result: [ClassObject] = []
  
  func isEmpty() -> Bool {
    if result.isEmpty {
      return true
    }
    return (result.first?.id as! Int) <= 0
  }
}

class ClassObject: EVObject {
  var id: NSNumber? = 0
  var name: NSString? = ""
  var photo: NSString? = ""
  var user_id: NSNumber? = 0
  var category_id: NSNumber? = 0
  var tag: NSString? = ""
  var price: NSNumber? = 0
  var address: NSString? = ""
  var longitude: NSNumber? = 0
  var latitude: NSNumber? = 0
  var phone_number: NSString? = ""
  var start_date: NSString? = ""
  var teacher: NSString? = ""
  var description_vi: NSString? = ""
  var created_at: NSString? = ""
  var updated_at: NSString? = ""
  var user_photo: NSString? = ""
  var user_name: NSString? = ""
  var is_like: NSNumber? = 0
  var number_like: NSNumber? = 0
  var number_share: NSNumber? = 0
  var number_comment: NSNumber? = 0
  var listComments: [CommentObject] = []
  
  var photos: [String] {
    var photos = [String]()
    if let photo = photo {
      let photoString = photo as String
      if photoString.contains(",") {
        photos = photoString.split(",")
      } else if photoString.contains(";") {
        photos = photoString.split(";")
      } else if !photoString.isEmpty {
        photos = [photoString]
      }
    }
    return photos
  }
}

class CommentResponse: BaseResponse {
  var result: [CommentObject] = []
  
  func isEmpty() -> Bool {
    if result.isEmpty {
      return true
    }
    return (result.first?.id as! Int) <= 0
  }
}

class CommentObject: EVObject {
  var id: NSNumber? = 0
  var classes_id: NSNumber? = 0
  var user_id: NSNumber? = 0
  var content: NSString? = ""
  var created_at: NSString? = ""
  var updated_at: NSString? = ""
  var user_name: NSString? = ""
  var user_photo: NSString? = ""
  var comment_time: NSString? = ""
}
