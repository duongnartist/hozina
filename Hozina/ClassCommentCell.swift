//
//  ClassCommentCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/16/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ClassCommentCell: UITableViewCell {
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var commentLabel: UILabel!
  
  func display(commentObject: CommentObject) {
    if let value = commentObject.user_photo {
      if let url = URL(string: value as String) {
        avatarImageView.kf.setImage(with: url)
      } else {
        avatarImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
      }
    } else {
      avatarImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
    }
    
    if let value = commentObject.user_name {
      nameLabel.text = value as String
    } else {
      nameLabel.text = ""
    }
    
    if let value = commentObject.content {
      commentLabel.text = value as String
    } else {
      commentLabel.text = ""
    }
  }
}
