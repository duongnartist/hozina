//
//  SubCategoryCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class SubCategoryCell: UICollectionViewCell {
  
  @IBOutlet weak var mainView: BorderCornerView!
  @IBOutlet weak var nameLabel: UILabel!
  
  func display(subCategoryObject: SubCategoryObject) {
    if let color = subCategoryObject.color_code {
      mainView.backgroundColor = UIColor(hexString: color as String)
    }
    if let name = subCategoryObject.name {
      nameLabel.text = name as String
    }
  }
  
}
