//
//  NotificationsViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/9/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  var data = [SystemNotification]()
  
  override func loadView() {
    super.loadView()
    print("NotificationsViewController -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    print("NotificationsViewController -> viewDidLoad")
//    tableView.rowHeight = UITableViewAutomaticDimension
//    tableView.estimatedRowHeight = 70
    tableView.rowHeight = 70
    for i in 0 ... 30 {
      let item = SystemNotification()
      item.avatar = "https://dantricdn.com/k:72b62a2872/2016/06/07/1-1465303829174/hot-girl-lao-goc-viet-gay-bao-mang-xa-hoi.jpg"
      item.image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIfeYv8-lLqq0HvS1f6pmRdEQxQnDTy3pKwH9bD6xF-2mhpBc5"
      item.name = "Name \(i)" as NSString
      item.dateTime = "10 min"
      item.read = i % 2 == 0
      data.append(item)
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    print("NotificationsViewController -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    print("NotificationsViewController -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print("NotificationsViewController -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    print("NotificationsViewController -> viewDidDisappear")
  }
  
  deinit {
    print("NotificationsViewController -> deinit")
  }
  
}

extension NotificationsViewController: UITableViewDataSource, UITableViewDelegate {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "SystemNotificationCell", for: indexPath) as! SystemNotificationCell
    cell.display(systemNotification: data[indexPath.row])
    return cell
  }
}
