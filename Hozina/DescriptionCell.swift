//
//  DescriptionCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZAlertController

class DescriptionCell: UICollectionViewCell {
  
  @IBOutlet weak var descriptionTextField: UITextField!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    descriptionTextField.delegate = self
  }
  
  func display(text: String) {
    descriptionTextField.text = text
  }
  
  func updateDescription(text: String) {
    let userObject = UserObject.userObject.clone()
    userObject.description_vi = text as NSString
    Network.updateProfile(userObject: userObject, response: { res in
//      EZAlertController.alert(R.string.main.alert_title(), message: res.getMessage(), acceptMessage: R.string.main.btn_ok(), acceptBlock: {
//        
//      })
    })
  }
}

extension DescriptionCell: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    let text = textField.text
    if (text?.length)! > 0 {
      updateDescription(text: text!)
    }
  }
}
