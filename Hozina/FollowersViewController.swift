//
//  FollowersViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/11/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class FollowersViewController: UIViewController {
  
  var followerData = [FollowerObject]()
  
  override func loadView() {
    super.loadView()
    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    print(" -> viewDidLoad")
    for i in 0 ..< 10 {
      let item = FollowerObject()
      item.name = "Name \(i)" as NSString
      item.photo = "https://images.pexels.com/photos/1609/person-woman-girl-sitting.jpg?h=350&auto=compress&cs=tinysrgb"
      item.isFriend = i % 3 == 0
      followerData.append(item)
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    print(" -> viewDidDisappear")
  }
  
  deinit {
    print(" -> deinit")
  }
  
  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
}

extension FollowersViewController: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return followerData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "FollowerCell", for: indexPath) as! FollowerCell
    cell.display(followerObject: followerData.get(at: indexPath.row)!)
    return cell
  }
}
