//
//  Follower.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/11/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class FollowerResponse: BaseResponse {
  var result: [FollowerObject] = []
}

class FollowerObject: EVObject {
  var id: NSNumber? = 0
  var name: NSString? = ""
  var photo: NSString? = ""
  var isFriend: Bool = false
}
