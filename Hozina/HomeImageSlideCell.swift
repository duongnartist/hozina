//
//  HomeImageSlideCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import Kingfisher
import ImageSlideshow

class HomeImageSlideCell: UICollectionViewCell {
  
  @IBOutlet weak var slideShow: ImageSlideshow!
  var kingfisherSources = [KingfisherSource]()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    slideShow.backgroundColor = UIColor.white
    slideShow.slideshowInterval = 5.0
    slideShow.pageControlPosition = PageControlPosition.insideScrollView
    slideShow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
    slideShow.pageControl.pageIndicatorTintColor = UIColor.black
    slideShow.contentScaleMode = UIViewContentMode.scaleAspectFill
    slideShow.activityIndicator = DefaultActivityIndicator()
    slideShow.currentPageChanged = { _ in
      
    }
    let recognizer = UITapGestureRecognizer(target: self, action: #selector(HomeImageSlideCell.didTap))
    slideShow.addGestureRecognizer(recognizer)
  }
  
  func display(images: [String]) {
    if kingfisherSources.isEmpty {
      for image in images {
        let kingfisherSource = KingfisherSource(urlString: image)
        kingfisherSources.append(kingfisherSource!)
      }
      slideShow.setImageInputs(kingfisherSources)
    }
  }
  
  func didTap() {
    
  }
}
