//
//  TabBarViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/8/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class TabBarViewController: UIViewController {
  
  @IBOutlet weak var tab0Button: UIButton!
  @IBOutlet weak var tab1Button: UIButton!
  @IBOutlet weak var tab2Button: UIButton!
  @IBOutlet weak var tab3Button: UIButton!
  @IBOutlet weak var tab4Button: UIButton!
  
  var onImages = [UIImage]()
  var offImages = [UIImage]()
  
  var tabButtons = [UIButton]()
  
  var selectedIndex = -1
  var selectTabBarIndexAction: ((_ index: Int) -> ())!
  
  override func loadView() {
    super.loadView()
    //    print("TabBarViewController -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print("TabBarViewController -> viewDidLoad")
    onImages = [#imageLiteral(resourceName: "Icon_Tab_Home_On"), #imageLiteral(resourceName: "Icon_Tab_Heart_On"), #imageLiteral(resourceName: "Icon_Tab_Hat_On"), #imageLiteral(resourceName: "Icon_Tab_Bell_On"), #imageLiteral(resourceName: "Icon_Tab_User_On")]
    offImages = [#imageLiteral(resourceName: "Icon_Tab_Home_Off"), #imageLiteral(resourceName: "Icon_Tab_Heart_Off"), #imageLiteral(resourceName: "Icon_Tab_Hat_Off"), #imageLiteral(resourceName: "Icon_Tab_Bell_Off"), #imageLiteral(resourceName: "Icon_Tab_User_Off")]
    tabButtons = [tab0Button, tab1Button, tab2Button, tab3Button, tab4Button]
    selectTab(index: 0)
    NotificationCenter.default.addObserver(self, selector: #selector(showMyProfile), name: NSNotification.Name("ShowMyProfile"), object: nil)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print("TabBarViewController -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print("TabBarViewController -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print("TabBarViewController -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print("TabBarViewController -> viewDidDisappear")
  }
  
  deinit {
    //    print("TabBarViewController -> deinit")
    NotificationCenter.default.removeObserver(self)
  }
  
  func showMyProfile() {
    selectTab(index: 4)
  }
  
  func selectTab(index: Int) {
    if selectedIndex != index {
      selectedIndex = index
      for i in 0 ..< tabButtons.count {
        let image = i == index ? onImages[i] : offImages[i]
        tabButtons[i].setImage(image, for: .normal)
      }
      if selectTabBarIndexAction != nil {
        selectTabBarIndexAction(index)
      }
    }
  }
  
  @IBAction func tab0ButtonTouchUpInside(_ sender: Any) {
    selectTab(index: 0)
  }
  
  @IBAction func tab1ButtonTouchUpInside(_ sender: Any) {
    selectTab(index: 1)
  }
  
  @IBAction func tab2ButtonTouchUpInside(_ sender: Any) {
    selectTab(index: 2)
  }
  
  @IBAction func tab3ButtonTouchUpInside(_ sender: Any) {
    selectTab(index: 3)
  }
  
  @IBAction func tab4ButtonTouchUpInside(_ sender: Any) {
    selectTab(index: 4)
  }
}
