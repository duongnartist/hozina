//
//  ClassNewCommentCell.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/16/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ClassNewCommentCell: UITableViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var commentTextView: BorderCornerTextView!
  var classObject: ClassObject!
  var textString = ""
  
  override func awakeFromNib() {
    super.awakeFromNib()
    commentTextView.delegate = self
    NotificationCenter.default.addObserver(self, selector: #selector(focusTextView), name: NSNotification.Name("NewComment"), object: nil)
  }
  
  func display(classObject: ClassObject) {
    self.classObject = classObject
    if let value = UserObject.userObject.photo {
      if let url = URL(string: value as String) {
        avatarImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Icon_Avatar_Default"))
      } else {
        avatarImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
      }
    } else {
      avatarImageView.image = #imageLiteral(resourceName: "Icon_Avatar_Default")
    }
    if textString.isEmpty {
      textString = commentTextView.text
    }
  }
  
  func focusTextView() {
    commentTextView.becomeFirstResponder()
  }
}

extension ClassNewCommentCell: UITextViewDelegate {
  
  func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    textView.text = ""
    return true
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    let content = textView.text
    if !(content?.isEmpty)! {
      Network.addComment(classObject: classObject, content: content!, response: { res in
        
      })
    }
    textView.text = textString
  }
}
