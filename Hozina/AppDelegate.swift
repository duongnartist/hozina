//
//  AppDelegate.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/8/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import FBSDKCoreKit
import FirebaseCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

  var window: UIWindow?
  let locationManager = CLLocationManager()
  var location: CLLocation!
  var thelatitude: Double = 0.0
  var thelongitude: Double = 0.0
  let defaults = UserDefaults.standard

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    print(NSLocalizedString("profile_my_collection", comment: ""))
    // Override point for customization after application launch.
    IQKeyboardManager.sharedManager().enable = true
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.requestAlwaysAuthorization()
    locationManager.startUpdatingLocation()
    if !CLLocationManager.locationServicesEnabled() {
      defaults.set(0.0, forKey: "latitude")
      defaults.set(0.0, forKey: "longitude")
    }
    FirebaseApp.configure()
    return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
  }
  
  func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let userLocation: AnyObject = locations[0]
    UIApplication.shared.statusBarStyle = .lightContent
    thelatitude = userLocation.coordinate.latitude
    thelongitude = userLocation.coordinate.longitude
    locationManager.stopUpdatingLocation()
    switch CLLocationManager.authorizationStatus() {
    case .notDetermined, .restricted, .denied:
      print("No access")
    case .authorizedAlways, .authorizedWhenInUse:
      defaults.set(userLocation.coordinate.latitude, forKey: "latitude")
      defaults.set(userLocation.coordinate.longitude, forKey: "longitude")
    }
  }
  
  func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
    return facebookDidHandle
  }

  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }


}

