//
//  ClassMenuTableViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/16/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ClassMenuTableViewController: UITableViewController {
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowShare" {
      if let shareViewController: ShareViewController = segue.destination as? ShareViewController {
        shareViewController.shareAction = { index in
          print("share \(index)")
        }
      }
    }
  }
  
  @IBAction func notificationSwitchValueChanged(_ sender: UISwitch) {
    if !sender.isOn {
      if let confirmViewController: ConfirmViewController = "ConfirmViewController".viewController(storyboard: "Main") as? ConfirmViewController {
        confirmViewController.selectAction = { index in
          if index == 0 {
            sender.isOn = true
          } else {
            
          }
        }
        confirmViewController.modalTransitionStyle = .crossDissolve
        confirmViewController.modalPresentationStyle = .overFullScreen
        present(confirmViewController, animated: true, completion: nil)
      }
    }
  }
}
