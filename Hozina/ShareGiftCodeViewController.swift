//
//  ShareGiftCodeViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class ShareGiftCodeViewController: UIViewController {
  
  @IBOutlet weak var codeTextView: UITextView!
  @IBOutlet weak var codeLabel: UILabel!
  var code = ""
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
    if let code = UserObject.userObject.introduce_code {
      self.code = code as String
      codeLabel.text = self.code
      codeTextView.text = String(format: "introduce_code".localized(), self.code)
//      codeTextView.text = Localize.
    } else {
      codeLabel.text = ""
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  @IBAction func facebookShareButtonTouchUpInside(_ sender: Any) {
  }
  
  @IBAction func instagramShareButtonTouchUpInside(_ sender: Any) {
  }
  
  @IBAction func skypeShareButtonTouchUpInside(_ sender: Any) {
    let urlSchema = "skype:?chat&topic=\(codeTextView.text)"
    if let url = URL(string: urlSchema) {
      
    }
  }
  
  @IBAction func zaloShareButtonTouchUpInside(_ sender: Any) {
  }
  
  
  @IBAction func otherButtonTouchUpInside(_ sender: Any) {
    let activityViewController = UIActivityViewController(activityItems:
      [codeTextView.text], applicationActivities: nil)
    let excludeActivities = [
      UIActivityType.message,
      UIActivityType.mail,
      UIActivityType.print,
      UIActivityType.copyToPasteboard,
      UIActivityType.assignToContact,
      UIActivityType.saveToCameraRoll,
      UIActivityType.addToReadingList,
      UIActivityType.postToFlickr,
      UIActivityType.postToTencentWeibo,
      UIActivityType.airDrop]
    activityViewController.excludedActivityTypes = excludeActivities;
    present(activityViewController, animated: true,
                          completion: nil)
  }
  
  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
}
