//
//  SettingsViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

  var settingsTableViewController: SettingsTableViewController!

  override func loadView() {
    super.loadView()
//    print(" -> loadView")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
//    print(" -> viewDidLoad")
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
//    print(" -> viewWillAppear")
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
//    print(" -> viewDidAppear")
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
//    print(" -> viewWillDisappear")
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
//    print(" -> viewDidDisappear")
  }

  deinit {
//    print(" -> deinit")
  }

  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    prepareEmbedMenu(for: segue)
  }

  func prepareEmbedMenu(for segue: UIStoryboardSegue) {
    if segue.identifier == "EmbedMenu" {
      if let vc: SettingsTableViewController = segue.destination as? SettingsTableViewController {
        vc.selectLanguageAction = { index in

        }
        vc.shareAction = {
          if let shareGiftCodeViewController: ShareGiftCodeViewController = "ShareGiftCodeViewController".viewController(storyboard: "Main") as? ShareGiftCodeViewController {
            self.navigationController?.pushViewController(shareGiftCodeViewController, animated: true)
          }
        }
        vc.selectMenuAction = { index in
          if index == 0 {
            if let managerViewController: ManagerViewController = "ManagerViewController".viewController(storyboard: "Main") as? ManagerViewController {
              self.navigationController?.pushViewController(managerViewController, animated: true)
            }
          } else if index == 2 {
            if let suggestsViewController: SuggestsViewController = "SuggestsViewController".viewController(storyboard: "Main") as? SuggestsViewController {
              self.navigationController?.pushViewController(suggestsViewController, animated: true)
            }
          } else if index == 4 {
            if let inviteFriendsViewController: InviteFriendsViewController = "InviteFriendsViewController".viewController(storyboard: "Main") as? InviteFriendsViewController {
              self.navigationController?.pushViewController(inviteFriendsViewController, animated: true)
            }
          } else if index == 7 {
            UserObject.userObject.clear()
            self.view.window?.rootViewController = "WelcomeViewController".viewController(storyboard: "Main")
          }
        }
        self.settingsTableViewController = vc
      }
    }
  }
}
