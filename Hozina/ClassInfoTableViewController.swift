//
//  ClassInfoTableViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/14/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import TagListView
import EZAlertController

class ClassInfoTableViewController: UITableViewController {

  @IBOutlet weak var addButton: BorderCornerButton!

  @IBOutlet weak var add0Button: UIButton!
  @IBOutlet weak var add1Button: UIButton!
  @IBOutlet weak var add2Button: UIButton!
  @IBOutlet weak var add3Button: UIButton!

  @IBOutlet weak var close0Button: UIButton!
  @IBOutlet weak var close1Button: UIButton!
  @IBOutlet weak var close2Button: UIButton!
  @IBOutlet weak var close3Button: UIButton!

  @IBOutlet weak var photo0ImageView: BorderCornerImageView!
  @IBOutlet weak var photo1ImageView: BorderCornerImageView!
  @IBOutlet weak var photo2ImageView: BorderCornerImageView!
  @IBOutlet weak var photo3ImageView: BorderCornerImageView!

  var categoryTitles = [String]()
  var categoryIds = [Int]()

  var selectedCategoryTitles = [String]()
  var selectedCategoryIds = [Int]()

  var addButtons = [UIButton]()
  var closeButtons = [UIButton]()
  var photoImageViews = [BorderCornerImageView]()

  var images = [UIImage]()
  var filleds = [false, false, false, false]

  var isFirst = true
  var greenColor = UIColor(hexString: "#0abf5d")
  var grayColor = UIColor(hexString: "#acacac")

  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var categoryTagListView: TagListView!
  @IBOutlet weak var priceTextField: UITextField!
  @IBOutlet weak var addressTextField: UITextField!
  @IBOutlet weak var phoneTextField: UITextField!
  @IBOutlet weak var openDateTextField: UITextField!
  @IBOutlet weak var teacherTextField: UITextField!
  @IBOutlet weak var descriptionTextView: UITextView!

  var requesting = false
  var selectedDate = Date()

  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
    descriptionTextView.make(cornerRadius: 6)
    descriptionTextView.make(borderWidth: 0.5, borderColor: UIColor.lightGray.cgColor)

    images = [UIImage(), UIImage(), UIImage(), UIImage()]

    addButtons = [add0Button, add1Button, add2Button, add3Button]
    closeButtons = [close0Button, close1Button, close2Button, close3Button]
    photoImageViews = [photo0ImageView, photo1ImageView, photo2ImageView, photo3ImageView]

    for category in CategoryObject.categoryData {
      categoryTitles.append(category.name! as String)
      categoryIds.append(category.id as! Int)
    }

    displayImages()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }

  deinit {
    //    print(" -> deinit")
  }

  func displayImages() {
    for i in 0 ..< images.count {
      closeButtons[i].isHidden = !filleds[i]
      addButtons[i].isHidden = filleds[i]
      photoImageViews[i].image = filleds[i] ? images[i] : nil
    }
  }

  @IBAction func addImage0ButtonTouchUpInside(_ sender: Any) {
    addImage(at: 0)
  }

  @IBAction func addImage1ButtonTouchUpInside(_ sender: Any) {
    addImage(at: 1)
  }

  @IBAction func addImage2ButtonTouchUpInside(_ sender: Any) {
    addImage(at: 2)
  }

  @IBAction func addImage3ButtonTouchUpInside(_ sender: Any) {
    addImage(at: 3)
  }

  func addImage(at index: Int) {
//    filleds[index] = true
//    displayImages()
    if let cameraViewController: CameraViewController = "CameraViewController".viewController(storyboard: "Main") as? CameraViewController {
      cameraViewController.currentIndex = index
      cameraViewController.images = images
      cameraViewController.filleds = filleds
      cameraViewController.doneAction = { images, filleds in
        self.images = images
        self.filleds = filleds
        self.displayImages()
      }
      present(cameraViewController, animated: true, completion: nil)
    }
  }

  @IBAction func addButtonTouchUpInside(_ sender: Any) {
    if selectedCategoryIds.count < 3 {
      let picker = ActionSheetStringPicker(title: "title_category".localized(), rows: categoryTitles, initialSelection: 0, doneBlock: { _, index, value in
        self.selectedCategoryIds.append(self.categoryIds.get(at: index)!)
        self.selectedCategoryTitles.append(self.categoryTitles.get(at: index)!)
        self.categoryIds.remove(at: index)
        self.categoryTitles.remove(at: index)
        self.categoryTagListView.addTag(self.selectedCategoryTitles.last!)
        self.updateCategories()
      }, cancel: { _ in

      }, origin: sender)
      picker?.setDoneButton(UIBarButtonItem(title: "btn_done".localized(), style: .plain, target: nil, action: nil))
      picker?.setCancelButton(UIBarButtonItem(title: "btn_cancel".localized(), style: .plain, target: nil, action: nil))
      picker?.show()
    }
  }

  func updateCategories() {
    let isFull = self.selectedCategoryIds.count >= 3
    self.addButton.backgroundColor = isFull ? self.grayColor : self.greenColor
    self.addButton.isEnabled = !isFull
  }

  func postClass() {
    guard requesting else {
      prepareInput(complete: { object, error, message in
        if error == -1 {
          self.show(requesting: &self.requesting)
          Network.upload(images: self.images, complete: { res in
            object.photo = res.getPhoto(from: Network.api.uploadUrl) as NSString
            Network.createClass(classObject: object, response: { res in
              self.hide(requesting: &self.requesting)
              EZAlertController.alert("alert_title".localized(), message: res._message, acceptMessage: "btn_ok".localized(), acceptBlock: {
                if res.isSuccess {
                  self.dismiss(animated: true, completion: nil)
                } else {

                }
              })
            })
          })
        } else {
          EZAlertController.alert("alert_title".localized(), message: message, acceptMessage: "btn_ok".localized(), acceptBlock: {
            switch error {
            case 0:
              self.nameTextField.becomeFirstResponder()
              break;
            default:
              break
            }
          })
        }
      })
      return
    }

  }

  func prepareInput(complete: @escaping (_ object: ClassObject, _ error: Int, _ message: String) -> ()) {
    let object = ClassObject()
    object.name = nameTextField.text! as NSString
    if (object.name?.length)! < 1 {
      complete(object, 0, "invalid_class_name".localized())
      return
    }

    object.price = priceTextField.text!.toDouble()! as NSNumber
    if (object.price?.doubleValue.isLess(than: 0))! {
      complete(object, 1, "invalid_class_price".localized())
      return
    }

    if !selectedCategoryIds.isEmpty {
      object.category_id = selectedCategoryIds.first! as NSNumber
    }

    var tag = ""
    for i in selectedCategoryIds {
      tag.append("\(i),")
    }
    object.tag = tag as NSString
    if tag.length < 1 {
      complete(object, 2, "invalid_class_address".localized())
      return
    }

    object.address = addressTextField.text! as NSString
    if (object.address?.length)! < 1 {
      complete(object, 3, "invalid_class_address".localized())
      return
    }

    object.phone_number = phoneTextField.text! as NSString
    if (object.phone_number?.length)! < 10 {
      complete(object, 4, "invalid_class_phone".localized())
      return
    }

    object.start_date = openDateTextField.text! as NSString
    if (object.start_date?.length)! < 1 {
      complete(object, 5, "invalid_class_date".localized())
      return
    }

    object.teacher = teacherTextField.text! as NSString
    if (object.teacher?.length)! < 1 {
      complete(object, 6, "invalid_class_teacher".localized())
      return
    }

    object.description_vi = descriptionTextView.text! as NSString
    if (object.description_vi?.length)! < 1 {
      complete(object, 7, "invalid_class_description".localized())
      return
    }

    complete(object, -1, "")
  }

  @IBAction func startDateButtonTouchUpInside(_ sender: Any) {
    ActionSheetDatePicker(title: openDateTextField.text, datePickerMode: .date, selectedDate: selectedDate, doneBlock: { picker, value, index in
      if let date = value as? Date {
        self.selectedDate = date
        self.openDateTextField.text = self.self.selectedDate.toString(format: "yyyy-MM-dd")
      }
    }, cancel: {
      ActionStringCancelBlock in return
    }, origin: openDateTextField).show()
  }

  @IBAction func delImage0ButtonTouchUpInside(_ sender: Any) {
    delImage(at: 0)
  }

  @IBAction func delImage1ButtonTouchUpInside(_ sender: Any) {
    delImage(at: 1)
  }

  @IBAction func delImage2ButtonTouchUpInside(_ sender: Any) {
    delImage(at: 2)
  }

  @IBAction func delImage3ButtonTouchUpInside(_ sender: Any) {
    delImage(at: 3)
  }

  func delImage(at index: Int) {
    images[index] = UIImage()
    filleds[index] = false
    displayImages()
  }
}

extension ClassInfoTableViewController: TagListViewDelegate {

  func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
    print("tagPressed \(title)")
  }

  func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
    print("tagRemoveButtonPressed \(title)")
    categoryTagListView.removeTag(title)
    let index = selectedCategoryTitles.index(of: title)
    selectedCategoryTitles.remove(at: index!)
    let id = selectedCategoryIds.get(at: index!)
    selectedCategoryIds.remove(at: index!)
    categoryIds.append(id!)
    categoryTitles.append(title)
    self.updateCategories()
  }

}

extension ClassInfoTableViewController: UITextViewDelegate, UITextFieldDelegate {

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }

  func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    if isFirst {
      isFirst = false
      textView.text = ""
      textView.textColor = UIColor.black
    }
    return true
  }

  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text.isEmpty {
      isFirst = true
      textView.text = "title_description".localized()
      textView.textColor = UIColor.lightGray
    }
  }

  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if textField == openDateTextField {
      textField.resignFirstResponder()
      startDateButtonTouchUpInside(textField)
    }
    return true
  }
}
