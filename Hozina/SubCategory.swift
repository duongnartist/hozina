//
//  SubCategory.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/10/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class SubCategoryResponse: BaseResponse {
  var result: [SubCategoryObject] = []
}

class SubCategoryObject: EVObject {
  var id: NSNumber? = 0
  var name: NSString? = ""
  var photo: NSString? = ""
  var category_id: NSNumber? = 0
  var color_code: NSString? = ""
  var is_color: NSNumber? = 0
  var created_at: NSString? = ""
  var updated_at: NSString? = ""
}
