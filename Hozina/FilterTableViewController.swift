//
//  FilterTableViewController.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/13/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class FilterTableViewController: UITableViewController {
  
  @IBOutlet weak var priceImage: UIImageView!
  @IBOutlet weak var dateImage: UIImageView!
  @IBOutlet weak var locationButton: UIButton!
  @IBOutlet weak var priceButton: UIButton!
  
  var selectedSortIndex = 1
  
  override func loadView() {
    super.loadView()
    //    print(" -> loadView")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    print(" -> viewDidLoad")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //    print(" -> viewWillAppear")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //    print(" -> viewDidAppear")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //    print(" -> viewWillDisappear")
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    //    print(" -> viewDidDisappear")
  }
  
  deinit {
    //    print(" -> deinit")
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row == 3 {
      selectSort(index: 0)
    } else if indexPath.row == 4 {
      selectSort(index: 1)
    } else {
      
    }
  }
  
  func selectSort(index: Int) {
    if selectedSortIndex != index {
      selectedSortIndex = index
      dateImage.image = selectedSortIndex == 0 ? #imageLiteral(resourceName: "Icon_Circle_Checked") : #imageLiteral(resourceName: "Icon_Circle_Unchecked")
      priceImage.image = selectedSortIndex == 1 ? #imageLiteral(resourceName: "Icon_Circle_Checked") : #imageLiteral(resourceName: "Icon_Circle_Unchecked")
    }
  }
  
  @IBAction func changeLocationButtonTouchUpInside(_ sender: Any) {
    
  }
  
  @IBAction func changePriceButtonTouchUpInside(_ sender: Any) {
    
  }
}
