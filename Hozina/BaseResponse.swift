//
//  BaseResponse.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/9/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class BaseResponse: EVObject {
  
  var code: NSNumber? = 0
  var message: NSString? = ""
  
  var _message: String {
    return message as String? ?? ""
  }
  
  var isSuccess: Bool {
    return code == 200
  }
  
//  func getMessage() -> String {
//    return message! as String
//  }
  
//  func isSuccess() -> Bool {
//    return code == 200
//  }
}

class UploadResponse: BaseResponse {
  var result: NSString? = ""
  
  func getImageUrls(from baseUrl: String) -> [String] {
    var images = [String]()
    if let result = self.result {
      let names = (result as String).split(",")
      for name in names {
        var url = name
        if !url.contains(baseUrl) {
          url = baseUrl + name
        }
        images.append(url)
      }
    }
    return images
  }
  
  func getPhoto(from baseUrl: String) -> String {
    var photo = ""
    if let result = self.result {
      let names = (result as String).split(",")
      for name in names {
        var url = name + ","
        if !url.contains(baseUrl) {
          url = baseUrl + name + ","
        }
        photo.append(url)
      }
    }
    if photo.length > 2 {
      photo = photo[0 ... photo.length - 2]
    }
    print("photo: \(photo)")
    return photo
  }
}
