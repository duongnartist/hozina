//
//  LocationManager.swift
//  HenHoVTC
//
//  Created by Nguyen Thanh Duong on 5/4/17.
//  Copyright © 2017 Nguyen Thanh Duong. All rights reserved.
//

import UIKit
import CoreLocation

public class LocationManager : NSObject, CLLocationManagerDelegate
{
    /// Properties
    private var locationManager: CLLocationManager = CLLocationManager()
    private var requestAuthHandler: ((_ authorized: Bool)->Void)?
    private var updatelocationHandler: ((_ lat: Double,_ lon: Double)->Void)?
    
    /// Get shared instance
    public static let shared = LocationManager()
    
    override init() {
        super.init()
        locationManager.delegate = self
    }
    
    /// Request authentication
    func requestAuthentication(_ completion: @escaping (_ authorized: Bool)->Void)
    {
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways:
                completion(true)
                break
            case .authorizedWhenInUse:
                completion(true)
                break
            case .denied:
                completion(false)
                break
            case .restricted:
                completion(false)
                break
            case .notDetermined:
                requestAuthHandler = completion
                locationManager.requestWhenInUseAuthorization()
                break
            }
            
        }
        else
        {
            completion(false)
        }
    }
    
    /// Start update location
    func startUpdateLocation(_ completion: @escaping (_ lat: Double,_ lon: Double)->Void)
    {
        updatelocationHandler = completion
        locationManager.startUpdatingLocation()
    }
    
    // MARK: - Location manager delegate
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            locationManager.stopUpdatingLocation()
            let location = locations.first!
            updatelocationHandler?(location.coordinate.latitude, location.coordinate.longitude)
            updatelocationHandler = nil
        }
        
    }
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            requestAuthHandler?(true)
            requestAuthHandler = nil
            break
        case .authorizedWhenInUse:
            requestAuthHandler?(true)
            requestAuthHandler = nil
            break
        case .denied:
            requestAuthHandler?(false)
            requestAuthHandler = nil
            break
        case .restricted:
            requestAuthHandler?(false)
            requestAuthHandler = nil
            break
        case .notDetermined:
            break
            
        }
        
    }
}
