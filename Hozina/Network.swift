//
//  Network.swift
//  MyCurva
//
//  Created by Nguyễn Dương on 7/2/17.
//  Copyright © 2017 jaafar serhal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import EVReflection

class Network {
  
  static let deviceId = 2
  static let pageSize = 20
  static var firebaseToken = ""
  static let errorFormat = "{\"code\": 400, \"message\": \"%@\", \"result\": [] }"
  
  static func downloadedFrom(url: URL, done: @escaping (UIImage) -> ()) {
    URLSession.shared.dataTask(with: url) { data, response, error in
      guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
        let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
        let data = data, error == nil,
        let image = UIImage(data: data)
      else { return }
      DispatchQueue.main.async { () -> () in
        done(image)
      }
    }.resume()
  }
  
  static func downloadedFrom(link: String, done: @escaping (UIImage) -> ()) {
    guard let url = URL(string: link) else { return }
    downloadedFrom(url: url, done: done)
  }
  
  struct api {
    static let baseUrl = "http://128.199.228.205/hozina/public"
    static let uploadUrl = "\(baseUrl)/upload/"
  }
  
  static func get(
    uri: String,
    params: [String: Any],
    response: @escaping (_ data: Data) -> ()
  ) {
    let url = api.baseUrl + uri
    let parameters = create(params: params, request: url)
    Alamofire.request(url, method: .get, parameters: parameters).responseData(completionHandler: { data in
      print(data.request?.url?.absoluteString ?? "")
      if data.result.isSuccess {
        response(data.data!)
      } else {
        let errorString = String(format: errorFormat, "request_error".localized())
        response(errorString.data(using: .utf8)!)
      }
    })
  }
  
  static func post(
    uri: String,
    params: [String: Any],
    response: @escaping (_ data: Data) -> ()
  ) {
    let url = api.baseUrl + uri
    let parameters = create(params: params, request: uri)
    Alamofire.request(url, method: .post, parameters: parameters).responseData(completionHandler: { data in
      print(data.request?.url?.absoluteString ?? "")
      if data.result.isSuccess {
        response(data.data!)
      } else {
        let errorString = String(format: errorFormat, "request_error".localized())
        response(errorString.data(using: .utf8)!)
      }
    })
  }
  
  static func create(
    params: [String: Any]?,
    request: String
  ) -> [String: Any] {
    var parameters = [String: Any]()
    if let params = params {
      parameters = params
    }
    return parameters
  }
  
  static func upload(images: [UIImage], complete: @escaping (_ response: UploadResponse) -> ()) {
    var params: [String: String] = [:]
    if let userId = UserObject.userObject.id, let apiToken = UserObject.userObject.ApiToken {
      params = ["UserID": "\(userId)", "ApiToken": "\(apiToken)"]
    }
    let path = "/api/users/uploadFile"
    Alamofire.upload(
      multipartFormData: { multipartFormData in
        var fileIndex = 0
        for image in images {
          let imageData = UIImageJPEGRepresentation(image, 0.5)
          if imageData != nil {
            fileIndex += 1
            multipartFormData.append(imageData!, withName: "FileNo\(fileIndex)", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
          }
        }
        for (key, value) in params {
          multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
        }
      },
      to: api.baseUrl + path,
      encodingCompletion: { encodingResult in
        switch encodingResult {
        case let .success(upload, _, _):
          upload.responseData(completionHandler: {
            response in
            let res = UploadResponse(data: response.data!)
            complete(res)
          })
        case let .failure(error):
          print(error)
          let res = UploadResponse()
          res.code = 401
          res.message = error.localizedDescription as NSString
          complete(res)
        }
      }
    )
  }
  
  // 1. Register & Login Facebook
  static func registerAndLoginFacebook(userObject: UserObject, response: @escaping (UserResponse) -> ()) {
    let uri = "/api/users/loginAndRegisterFacebook"
    let params: [String: Any] = [
      "email": userObject.email!,
      "facebook_id": userObject.facebook_id!,
      "phone_number": userObject.phone_number!,
      "photo": userObject.photo!,
      "name": userObject.name!,
      "address": userObject.address!,
      "device_type": deviceId,
      "token_firebase": firebaseToken
    ]
    post(uri: uri, params: params, response: {
      data in
      let res = UserResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      if res.isSuccess {
        UserObject.userObject = res.result!
        UserObject.userObject.password = userObject.password
        UserObject.userObject.save()
      }
      response(res)
    })
  }
  
  // 2. Register
  static func register(userObject: UserObject, response: @escaping (UserResponse) -> ()) {
    let uri = "/api/users/activeAccount"
    let params: [String: Any] = [
      "password": userObject.password!,
      "phone_number": userObject.phone_number!,
      "device_type": deviceId,
      "token_firebase": firebaseToken
    ]
    post(uri: uri, params: params, response: {
      data in
      let res = UserResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      if res.isSuccess {
        UserObject.userObject = res.result!
        UserObject.userObject.password = userObject.password
        UserObject.userObject.save()
      }
      response(res)
    })
  }
  
  // 3. Login
  static func login(userObject: UserObject, response: @escaping (UserResponse) -> ()) {
    let uri = "/api/users/login"
    let params: [String: Any] = [
      "phone_number": userObject.phone_number!,
      "password": userObject.password!,
      "device_type": deviceId,
      "token_firebase": firebaseToken
    ]
    post(uri: uri, params: params, response: {
      data in
      let res = UserResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      if res.isSuccess {
        UserObject.userObject = res.result!
        UserObject.userObject.password = userObject.password
        UserObject.userObject.save()
      }
      response(res)
    })
  }
  
  // 5. Get all categories
  static func getAllCategories(response: @escaping (CategoryResponse) -> ()) {
    let uri = "/api/data/getAllCategories"
    let params: [String: Any] = [:]
    get(uri: uri, params: params, response: {
      data in
      let res = CategoryResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 6. Get sub category
  static func getSubCategory(categoryObject: CategoryObject, response: @escaping (SubCategoryResponse) -> ()) {
    let uri = "/api/data/getSubCategory"
    let params: [String: Any] = [
      "category_id": categoryObject.id!
    ]
    get(uri: uri, params: params, response: { data in
      let res = SubCategoryResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 7. Create class
  static func createClass(classObject: ClassObject, response: @escaping (ClassResponse) -> ()) {
    let uri = "/api/data/createClass"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "ApiToken": UserObject.userObject.ApiToken!,
      "name": classObject.name!,
      "photo": classObject.photo!,
      "category_id": classObject.category_id!,
      "tag": classObject.tag!,
      "price": classObject.price!,
      "address": classObject.address!,
      "longitude": classObject.longitude!,
      "latitude": classObject.latitude!,
      "phone_number": classObject.phone_number!,
      "start_date": classObject.start_date!,
      "teacher": classObject.teacher!,
      "description": classObject.description_vi!
    ]
    post(uri: uri, params: params, response: {
      data in
      let res = ClassResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 8. Get list class
  static func getListClass(categoryObject: CategoryObject, page_number: Int, response: @escaping (ClassResponse) -> ()) {
    let uri = "/api/data/getListClass"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "category_id": categoryObject.id!,
      "page_size": pageSize,
      "page_number": page_number
    ]
    get(uri: uri, params: params, response: {
      data in
      let res = ClassResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 9. Add favorite
  static func addFavorite(classObject: ClassObject, response: @escaping (BaseResponse) -> ()) {
    let uri = "/api/data/addFavorite"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "ApiToken": UserObject.userObject.ApiToken!,
      "classes_id": classObject.id!
    ]
    post(uri: uri, params: params, response: {
      data in
      let res = BaseResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 10. Add comment
  static func addComment(classObject: ClassObject, content: String, response: @escaping (BaseResponse) -> ()) {
    let uri = "/api/data/addComment"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "ApiToken": UserObject.userObject.ApiToken!,
      "classes_id": classObject.id!,
      "content": content
    ]
    post(uri: uri, params: params, response: {
      data in
      let res = BaseResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 11. Get list class by sub category
  static func getListClass(subCategoryObject: SubCategoryObject, pageNumber: Int, response: @escaping (ClassResponse) -> ()) {
    let uri = "/api/data/getListClassBySubCategory"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "sub_category_id": subCategoryObject.id!,
      "page_size": pageSize,
      "page_number": pageNumber
    ]
    get(uri: uri, params: params, response: {
      data in
      let res = ClassResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 12. Get class details
  static func getClassDetails(classObject: ClassObject, response: @escaping (ClassResponse) -> ()) {
    let uri = "/api/data/getClassDetails"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "class_id": classObject.id!
    ]
    get(uri: uri, params: params, response: { data in
      let res = ClassResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 13. Get list comment
  static func getListComments(classObject: ClassObject, pageNumber: Int, response: @escaping (CommentResponse) -> ()) {
    let uri = "/api/data/getListComments"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "ApiToken": UserObject.userObject.ApiToken!,
      "class_id": classObject.id!,
      "page_size": pageSize,
      "page_number": pageNumber
    ]
    get(uri: uri, params: params, response: { data in
      let res = CommentResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 14. Get list by user
  static func getListClassByUser(userObject: UserObject, categoryObject: CategoryObject, classType: Int, pageNumber: Int, response: @escaping (ClassResponse) -> ()) {
    var categoryId = 0
    if let id = categoryObject.id {
      categoryId = id as! Int
    }
    let uri = "/api/data/getListClassByUser"
    let params: [String: Any] = [
      "UserID": userObject.id!,
      "class_type": classType,
      "category_id": categoryId,
      "page_size": pageSize,
      "page_number": pageNumber
    ]
    get(uri: uri, params: params, response: { data in
      let res = ClassResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 15. Add user favorite
  static func addUserFavorite(userObject: UserObject, response: @escaping (BaseResponse) -> ()) {
    let uri = "/api/data/addUserFavorite"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "ApiToken": UserObject.userObject.ApiToken!,
      "user_id": userObject.id!
    ]
    post(uri: uri, params: params, response: { data in
      let res = BaseResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 16. Add share user
  static func addShareUser(userObject: UserObject, response: @escaping (BaseResponse) -> ()) {
    let uri = "/api/data/addShareUser"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "ApiToken": UserObject.userObject.ApiToken!,
      "user_id": userObject.id!
    ]
    post(uri: uri, params: params, response: { data in
      let res = BaseResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 17. Add share class
  static func addShareClass(classObject: ClassObject, response: @escaping (BaseResponse) -> ()) {
    let uri = "/api/data/addShareClass"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "ApiToken": UserObject.userObject.ApiToken!,
      "class_id": classObject.id!
    ]
    post(uri: uri, params: params, response: { data in
      let res = BaseResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 18. Get user profile
  static func getUserProfile(userObject: UserObject, response: @escaping (UserResponse) -> ()) {
    let uri = "/api/users/getUserProfile"
    let params: [String: Any] = [
      "UserID": userObject.id!
    ]
    get(uri: uri, params: params, response: { data in
      let res = UserResponse(data: data)
      if res.isSuccess && res.result?.id == UserObject.userObject.id {
        let password = UserObject.userObject.password
        UserObject.userObject = res.result!
        UserObject.userObject.ApiToken = res.result?.remember_token
        UserObject.userObject.password = password
      }
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      response(res)
    })
  }
  
  // 19. Update profile
  static func updateProfile(userObject: UserObject, response: @escaping (UserResponse) -> ()) {
    let uri = "/api/users/updateProfile"
    let params: [String: Any] = [
      "UserID": UserObject.userObject.id!,
      "ApiToken": UserObject.userObject.ApiToken!,
      "name": userObject.name!,
      "photo": userObject.photo!,
      "address": userObject.address!,
      "description_vi": userObject.description_vi!,
      "birthday": userObject.birthday!,
      "email": userObject.email!,
    ]
    post(uri: uri, params: params, response: { data in
      let res = UserResponse(data: data)
      print("\n\(uri)")
      print("\n\(params)")
      print("\n\(res)\n")
      if res.isSuccess {
        UserObject.userObject = res.result!
        UserObject.userObject.password = userObject.password
        UserObject.userObject.save()
      }
      response(res)
    })
  }
  
}
